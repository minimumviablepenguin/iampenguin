﻿using UnityEngine;
using System.Collections;
using HGC;

public class FakeCam : ManagedBehaviour {

	// Use this for initialization
	protected override void OnGameLoaded () {
	    if(GetComponent<SteamVR_Camera>() == null)
        {
            gameObject.AddComponent<MouseLook>();
            gameObject.AddComponent<Camera>();
        }
	}
	
}
