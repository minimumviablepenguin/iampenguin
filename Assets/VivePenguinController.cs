﻿using UnityEngine;
using System.Collections;
using HGC;

public class VivePenguinController : ManagedBehaviour {

    [SerializeField]
    private GameObject penguinRoot;
    [SerializeField]
    private GameObject penguinHead;
    [SerializeField]
    private GameObject penguinEgg;

    // Use this for initialization
    protected override void OnStart()
    {
	
	}

    protected override void OnGameLoaded()
    {
        Log.Info("Penguin", "Penguin rrrrready!");
    }

    // Update is called once per frame
    void Update () {
	
	}
}
