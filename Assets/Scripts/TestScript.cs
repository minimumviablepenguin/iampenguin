﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class TestScript : MonoBehaviour {

	//Live scale
	private float scale = 10;


	//Variables for the particle system
	private List<float> startSpeed = new List<float>();
	private List<float> startLife = new List<float>();
	private List<float> startSize = new List<float>();
	private List<float> startRate = new List<float>();
	private List<float> startGrav = new List<float>();
	private List<float> startMax = new List<float>();

	// Use this for initialization
	void Start () {
	
		ParticleSystem p = GetComponent<ParticleSystem>();
		float trigger = 1 + Time.deltaTime;
		
		if (trigger > 5){
			UpdateScale(p, 1);
		}

	}
	
	// Update is called once per frame
	void Update () {
	


	}

	//Update particle system
	void UpdateScale(ParticleSystem p, int index)
	{
	    p.startSpeed = startSpeed[index] * scale;
		p.startLifetime = startLife[index] * scale;
		p.startSize = startSize[index] * scale;
		p.emissionRate = startRate[index] * scale;
		p.gravityModifier = startGrav[index] * scale;
		p.maxParticles = (int)(startMax[index] * scale);
	}
	/*
	//Possible rotation function
	public void Rotate (Vector3 axis, float angle)
	{
		Space relativeTo = Space.Self;
		this.Rotate (axis, angle, relativeTo);
	}*/
}
