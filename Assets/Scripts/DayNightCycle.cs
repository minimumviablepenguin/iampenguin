﻿using UnityEngine;
using System.Collections;

public class DayNightCycle : MonoBehaviour {

    public float TimeOfDay,DegreesInTime,SunHeight,TimeModifier;
    //Time of day is supposed to give you the time in a normal 24-hour clock format.
    //Degrees in time is a modifier to show how many degrees the sun has moved and how many minutes that means.
    //Time modifier is changes the speed of the passing of time on an X to 1 ratio.
    GameObject dirLight; //

	// Use this for initialization
	void Start () {
        dirLight = gameObject;
        // I tell the script that the Directional Light is what it's gonna affect.
    }
	
	// FixedUpdate because why not.
	void FixedUpdate ()
    {

        dirLight.transform.Rotate(new Vector3(SunHeight, TimeModifier * Time.deltaTime, 0));
        //In the update I tell it to rotate the light along the Y axis, 

        TimeIsOneCircle(); //made mostly for the sake of not having the y-rotation value go past fifty thousand.
                           //timeUpdate();
        CheckSeconds(); //Attempts to print a debug.log of how many seconds have 
    }

    private void CheckSeconds()
    {
        DegreesInTime = dirLight.transform.rotation.y * 60 / 4;
        /*
        So here I'm trying to calculate seconds out from how many degrees the sun has moved. 
        Not sure exactly how to do it. I understood that one degree is 4 minutes. 86400/360 = 240 = 4 minutter
        So the plan is to say one degree is 240 seconds. But I'm not entirely sure how to do so.
        Especially since the degree is changed by Time.Deltatime (which is the time, in seconds, since the last update)
        
        What if I just do time.time instead? That'd count seconds since the scene started?
        But 
        */
       // Debug.Log(DegreesInTime + " seconds have passed.");
    }

    private void timeUpdate()
    {
        //So one full day cycle is 86400 seconds. 86400 / 360 is 240, and 240 seconds is 4 minutes.
        //No I'm going to try and make it so that the code parses this as a time stamp. 
        var SecondsInACycle = 86400;
        var Minute = SecondsInACycle / 60; 
        var Hour = Minute / 60;
        var Day = Hour / 24;

        //So there's 86400 secs in one cycle. What I want to do is to see how many seconds have passed
        int SecondsPassed, MinutesPassed, HoursPassed, DaysPassed;
        //DaysPassed = ; //Counts the amount of days in the total amount of in-game seconds that have passed
        //HoursPassed = ; //SAme, but for Hours
        //MinutesPassed = ; // -||-
        //SecondsPassed = ; // -||-
        
        
        //TimeOfDay = ;

        //Debug.Log(DaysPassed + " days, " + HoursPassed + " hours, " + MinutesPassed + " minutes, and " + SecondsPassed +" seconds.");

    }
    
    private void TimeIsOneCircle()
    {
        if (DegreesInTime > 360)
        {
            DegreesInTime = DegreesInTime - 360;
        }
    }
}
