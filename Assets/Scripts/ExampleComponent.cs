﻿using UnityEngine;
using System.Collections;
using HGC;

public class ExampleComponent : ManagedBehaviour
{
    // runs when component is first instantiated, other components are coming online unpredictably
    protected override void OnAwake()
    {

    }

    // runs after everything in the current scene is instantiated
    protected override void OnStart()
    {

    }

    // runs after everything is setup
    protected override void OnGameLoaded()
    {
	
	}

    // main game loop, runs more often than the physics update
    public void Update()
    {

    }

    // physics update
    public void FixedUpdate()
    {

    }
}
