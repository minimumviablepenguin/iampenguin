﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HGC;

public class PenguinCamControls : ManagedBehaviour
{

    [SerializeField]
    private GameObject Penguin;

    [SerializeField]
    private GameObject PenguinHead;

    [SerializeField]
    private GameObject Egg;

    [SerializeField]
    private GameObject SteamVR;

    [SerializeField]
    private AudioClip DepressedPenguinSound;

    private bool hasLost;

    private Penguin[] penguins;

    // Use this for initialization
    protected override void OnStart()
    {

        penguins = (Penguin[])Object.FindObjectsOfType(typeof(Penguin));
    }

    // Update is called once per frame
    void Update()
    {
        if (SteamVR.transform.position.y != 0f)
        {
            //Log.Info("Heellllo", "why are you 1: " + (SteamVR.transform.position.y-1));
            // rescale penguin
            Penguin.transform.localScale = new Vector3(1f, SteamVR.transform.position.y - 1 + 0.001f, 1f);

            foreach (var penguin in penguins)
            {
                penguin.transform.localScale = new Vector3(1f, SteamVR.transform.position.y - 1 + 0.001f, 1f);
            }
        }

        var look = Quaternion.LookRotation(Vector3.Normalize((SteamVR.transform.position - penguins[0].transform.position)), Vector3.up);
        // rotate to face player direction
        var rot = look;
        rot.x = 0;
        rot.z = 0;
        penguins[0].transform.rotation = rot;

        // move penguin translation
        Penguin.transform.rotation = new Quaternion(0f, SteamVR.transform.rotation.y, 0f, SteamVR.transform.rotation.w);
        Penguin.transform.position = new Vector3(SteamVR.transform.position.x, SteamVR.transform.position.y - 1, SteamVR.transform.position.z);

        // clamp vr head Y
  
        var steamCam = PenguinHead.transform.position;
        steamCam.y = SteamVR.transform.position.y;
        PenguinHead.transform.position = steamCam;


        // lose game
        if (Vector3.Distance(penguins[0].transform.position, SteamVR.transform.position) > 5.0f)
        {
            if (hasLost == false)
            {
                hasLost = true;
                StartCoroutine(LoseGame());
                StartCoroutine(RestartGame());
            }
        }
    }

    IEnumerator LoseGame()
    {
        AudioManager.PlaySound("FX/Penguin/Depressed", gameObject);
        while (true)
        {
            Egg.transform.position += (Penguin.transform.forward * (Time.deltaTime * 0.5f));
            Egg.transform.rotation = new Quaternion((Egg.transform.rotation.x + (Time.deltaTime * 0.5f)), Egg.transform.rotation.y, Egg.transform.rotation.z, Egg.transform.rotation.w);
            yield return null;
        }
    }

    IEnumerator RestartGame()
    {
        yield return new WaitForSeconds(3f);
        Application.LoadLevel(0);
    }
}
