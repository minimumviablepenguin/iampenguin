﻿using System;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using System.Collections.Generic;

/* in the Editor we want to route calls to the mirrored class in the managed Editor plugin - Log.dll, since...
 * this retains Unity's link in the Editor's console to the correct stack frame (basically whenever you double click on the console log it takes you to the correct file & line) but...
 * in builds we want the compiler to strip out invocations to this class' methods by using the 'Conditional Attribute', and so...
 * remove garbage collection churn caused by all the immutable string concatenations in those code lines e.g. Log.Warn("GameLogic", "The value assigned to: '" + AnEnum.ToString() + "' was not valid: '" + value +  "'");
 * */

#if !UNITY_EDITOR
namespace HGC
{
	// DO NOT CHANGE THIS FILE! Or mirror the change in the managed editor dll - 'Log.dll'
	public class Log
	{
		public static readonly string CollabPrefix = "HGC.";

		// You don't need to add new categories here, since you add new categories whenever you pass a unlisted category through a Logging method.
		public enum Category
		{
			GAME // default, like an evil General category :< Use responsibly
			,ACHIEVEMENT
			,AI
			,BILLING
			,BUNDLE
			,CAMERA	
			,EDITOR
			,MULTIPLAYER
			,NETWORK
			,PLUGIN
			,RULES
			,STORAGE
			,UI		
		}

		[Conditional("DEBUG")]
		public static void Info(string message, Object context = null)
		{
			string category = Category.GAME.ToString();

			if (Instance.IsCategoryActive(category))
				UnityEngine.Debug.Log(Time.realtimeSinceStartup + " " + category.ToUpper() + ": " + message, context);
		}

		[Conditional("DEBUG")]
		public static void Warn(string message, Object context = null)
		{
			string category = Category.GAME.ToString();

			if (Instance.IsCategoryActive(category))
				UnityEngine.Debug.LogWarning(Time.realtimeSinceStartup + " " + category.ToUpper() + ": " + message, context);
		}

		[Conditional("DEBUG")]
		public static void Error(string message, Object context = null)
		{
			string category = Category.GAME.ToString();

			UnityEngine.Debug.LogError(Time.realtimeSinceStartup + " " + category.ToUpper() + ": " + message, context);
		}

		[Conditional("DEBUG")]
		public static void Info(string category, string message, Object context = null)
		{
			if(Instance.IsCategoryActive(category))
				UnityEngine.Debug.Log(Time.realtimeSinceStartup + " " + category.ToUpper() + ": " + message, context);
		}

		[Conditional("DEBUG")]
		public static void Warn(string category, string message, Object context = null)
		{
			if (Instance.IsCategoryActive(category))
				UnityEngine.Debug.LogWarning(Time.realtimeSinceStartup + " " + category.ToUpper() + ": " + message, context);
		}

		[Conditional("DEBUG")]
		public static void Error(string category, string message, Object context = null)
		{
			UnityEngine.Debug.LogError(Time.realtimeSinceStartup + " " + category.ToUpper() + ": " + message, context);
		}


        [Conditional("UNITY_EDITOR")]
		public static void Assert(bool isConditionTrue, string category, string message, Object context = null)
		{
#if UNITY_EDITOR
			if (!isConditionTrue)
			{
				if(UnityEditor.EditorUtility.DisplayDialog(" " + category + " Assertion!", message, "We assumed too much..."))
				{
					Error(category, "ASSERTION: " + message, context);
					Debug.Break();
				}
			}
#endif
		}


        public static Dictionary<string, bool> ActiveLogCategories
		{
			get { return Instance._activeLogCategories; }
		}

		private Dictionary<string, bool> _activeLogCategories;
		private string[] _activeCategorieStrings;
		private static Log _instance;

		private static Log Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				_instance = new Log();;
				_instance._activeCategorieStrings = Enum.GetNames(typeof(Category));

				_instance._activeLogCategories = new Dictionary<string, bool>();

				foreach (string category in _instance._activeCategorieStrings)
				{
					_instance._activeLogCategories.Add(category.ToUpper(), true);
				}

				return _instance;
			}
		}

		private bool IsCategoryActive(string category)
		{
			category = category.ToUpper();

			if(!_activeLogCategories.ContainsKey(category))
				_activeLogCategories.Add(category, true);

#if UNITY_EDITOR
			bool logPreference = UnityEditor.EditorPrefs.GetBool(CollabPrefix + "Debug." + category, true);
			_activeLogCategories[category] = logPreference;
#endif

			return _activeLogCategories[category];
		}
	}
}
#endif
