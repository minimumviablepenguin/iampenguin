using System;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.ProjectWindowCallback;

internal class EndNameEdit : EndNameEditAction
{
	#region implemented abstract members of EndNameEditAction
	public override void Action (int instanceId, string pathName, string resourceFile)
	{
		AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(instanceId), AssetDatabase.GenerateUniqueAssetPath(pathName));
	}

	#endregion
}

/// <summary>
/// Scriptable object window.
/// </summary>
public class ScriptableObjectWindow : EditorWindow
{
    private static string _lastName;
    private int _selectedIndex;
	private static string[] _names;	
	private static Type[] _types;
	
	private static Type[] Types
	{ 
		get { return _types; }
		set
		{
			_types = value;
			_names = _types.Select(t => t.FullName).ToArray();
		}
	}

    public static void Init(Type[] scriptableObjects)
	{
		Types = scriptableObjects;
        ScriptableObjectWindow window = (ScriptableObjectWindow)EditorWindow.GetWindow(typeof(ScriptableObjectWindow));
		window.Show();

        if(!_names.IsNullOrEmpty())
            _lastName = GetUnqualifiedTypeName(_names[0]);
    }

    public static string GetUnqualifiedTypeName(string qualifiedType)
    {
        if (qualifiedType.Contains("."))
            return qualifiedType.Remove(0, qualifiedType.LastIndexOf(".") + 1);
        else
            return qualifiedType;
    }

	public void OnGUI()
	{
		GUILayout.Label("ScriptableObject Class", EditorStyles.boldLabel);

        if (_names.IsNullOrEmpty())
            return;

        _lastName = EditorGUILayout.TextField("Asset Name", _lastName);
        int newIndex = EditorGUILayout.Popup("Type", _selectedIndex, _names);

        if(newIndex != _selectedIndex)
        {
            _selectedIndex = newIndex;
            _lastName = GetUnqualifiedTypeName(_names[_selectedIndex]);
        }

		if (GUILayout.Button("Create Asset"))
		{
			var asset = ScriptableObject.CreateInstance(_types[_selectedIndex]);
			ProjectWindowUtil.StartNameEditingIfProjectWindowExists(
				asset.GetInstanceID(),
				ScriptableObject.CreateInstance<EndNameEdit>(),
				string.Format("{0}.asset", _lastName),
				AssetPreview.GetMiniThumbnail(asset), 
				null);

			Close();
		}
	}
}
#endif