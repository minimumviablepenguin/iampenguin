﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;

namespace HGC
{
    /// <summary>
    /// Action thread (parameterless) - Note you cannot call unity methods from outside of the main thread, but you can in a coroutine
    /// </summary>
    public class ActionThread
    {
        private bool _isDone = false;
        Thread _thread;

        public bool IsDone { get { return _isDone;  } }
        public Thread RunnerThread { get { return _thread; } }

		public ActionThread(Action action, Action<bool> callback)
		{
			_thread = new Thread(() => {
				if(action != null)
					action();

				_isDone = true;

				if(callback != null)
					callback(_isDone);
			});

            _thread.Start();
		}
	
		public IEnumerator WaitForComplete()
		{
			while (!_isDone)
				yield return null;
		}
	}

	/// <summary>
	/// Action thread (1 parameter) - Note you cannot call unity methods from outside of the main thread, but you can in a coroutine
	/// </summary>
	public class ActionThread<T>
	{	
		private bool _isDone = false;
        Thread _thread;

        public bool IsDone { get { return _isDone; } }
        public Thread RunnerThread { get { return _thread; } }

        public ActionThread(Action<T> anExpensiveMethod, T methodParam, Action<bool> callback)
		{
            _thread = new Thread(() => {
				if (anExpensiveMethod != null)
					anExpensiveMethod(methodParam);

				_isDone = true;

				if (callback != null)
					callback(_isDone);
			});

            _thread.Start();
		}
	
		public IEnumerator WaitForComplete()
		{
			while (!_isDone)
				yield return null;
		}
	}

	/// <summary>
	/// Action thread (1 parameter, 1 return value) - Note you cannot call unity methods from outside of the main thread, but you can in a coroutine
	/// </summary>
	public class ActionThread<Input, Output>
	{
		private bool _isDone = false;
        Thread _thread;

        public bool IsDone { get { return _isDone; } }
        public Thread RunnerThread { get { return _thread; } }

        public ActionThread(Func<Input, Output> anExpensiveMethod, Input methodParam, Action<bool, Output> callback)
		{
            _thread = new Thread(() =>
			{
				if (anExpensiveMethod == null)
					return;
			
				Output result = anExpensiveMethod(methodParam);

				_isDone = true;

				if (callback != null)
					callback(_isDone, result);
			});

            _thread.Start();
		}

		public IEnumerator WaitForComplete()
		{
			while (!_isDone)
				yield return null;
		}
	}
}