﻿using UnityEngine;
using System.Collections;

namespace HGC.Storage
{
	public interface IProcess
	{
		void Process();
	}
}

