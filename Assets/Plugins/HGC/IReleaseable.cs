﻿using UnityEngine;
using System.Collections;

public interface IReleaseable
{
	void Release();
}
