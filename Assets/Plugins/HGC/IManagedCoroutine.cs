﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace HGC
{
    public interface IManagedCoroutine
    {
        bool IsPaused { get; }
        bool IsStopped { get; }

        void PauseCoroutine();
        void ResumeCoroutine();

        Coroutine StartPausableCoroutine(IEnumerator coroutine);
        Coroutine PauseForSeconds(float seconds);
        Coroutine WaitForSeconds(float seconds, Func<bool> breakOut = null);
        Coroutine AfterSeconds(float seconds, Action callback, Func<bool> breakOut = null);
    }
}