﻿using UnityEngine;
using System.Collections;

namespace HGC
{
    public class GameLogicManager : Singleton<GameLogicManager>
    {
        [SerializeField]
        private FrameworkManager _framework;

        public override void OnInitialise()
        {
            if(_framework == null)
            {
                FrameworkManager framework = FindObjectOfType<FrameworkManager>();
                if (framework == null)
                {
                    var frameworkPath = Resources.Load("HGC/Prefabs/Framework") as GameObject;

                    Log.Assert(frameworkPath != null, "Project", "HGC framework prefab was not located at the hardcoded path");
                    
                    var frameworkObj = Instantiate(frameworkPath);
                    _framework = frameworkObj.GetComponent<FrameworkManager>();
                    frameworkObj.name = typeof(FrameworkManager).Name;
                    gameObject.transform.SetParent(_framework.ManagersParent.transform);
                }
            }
        }
    }
}
