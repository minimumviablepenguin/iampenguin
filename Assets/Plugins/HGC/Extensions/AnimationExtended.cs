using System;
using UnityEngine;
using System.Collections;

public static class AnimationExtensions
{

    public static AnimationState GetClipAt(this Animation animation, int index)
    {
	    if (!animation)
		    return null;

	    int counter = 0;
        foreach (AnimationState state in animation)
        {
            if (counter++ == index)
                return state;
        }

        return null;
    }

    public static AnimationClip GetClipClipAt(this Animation animation, int index)
    {
        return animation.GetClipAt(index).clip;
    }

    public static int GetClipList(this Animation animation, out string[] clipList)
    {
        int counter = 0;
        clipList = new string[animation.GetClipCount()];

        foreach (AnimationState state in animation)
        {
            clipList[counter] = state.name;

            ++counter;
        }

        return counter;
    }
}
