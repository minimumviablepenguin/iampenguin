﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Linq;
using HGC.Events;

public static class GameObjectExtensions
{
    /// <summary>
	/// Gets or add a component. Usage example:
	/// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
	/// </summary>
	static public T GetOrAddComponent<T>(this Component child) where T : Component
    {
        T result = child.GetComponent<T>();
        if (result == null)
        {
            result = child.gameObject.AddComponent<T>();
        }
        return result;
    }

    public static GameObject FindGameObjectChild(this GameObject gameObject, string path)
    {
        var split = path.Split('/').CreateList();
        if (gameObject != null)
        {
            return FindChild(gameObject, split);
        }

        return null;
    }

    public static GameObject FindChild(this GameObject obj, string name)
    {
        GameObject returnObject = null;
        Transform transform = obj.transform;

        foreach (Transform trs in transform)
        {
            if (trs.gameObject.name == name)
            {
                returnObject = trs.gameObject;
                break;
            }

            returnObject = trs.gameObject.FindChild(name);

            if (returnObject)
                break;
        }

        return returnObject;
    }

    private static GameObject FindChild(GameObject gameObject, List<string> split)
    {
        var transform = gameObject.transform;
        for (int i = 1; i < split.Count; i++)
        {
            transform = transform.FindChild(split[i]);
        }

        return transform.gameObject;
    }

    public static GameObject FindGameObject(string path)
    {
        var split = path.Split('/').CreateList();
        var gameObject = GameObject.Find(split[0]);
        if (gameObject != null)
        {
            return FindChild(gameObject, split);
        }

        return null;
    }
}