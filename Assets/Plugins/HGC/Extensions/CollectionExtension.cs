﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class CollectionExtension
{
    public static void Move(this IList list, int iIndexToMove, int direction)
    {
        //up
        if (direction == 1 && iIndexToMove > 0)
        {

            var old = list[iIndexToMove - 1];
            list[iIndexToMove - 1] = list[iIndexToMove];
            list[iIndexToMove] = old;
        }
        else if (direction != 1 && iIndexToMove < list.Count - 1)
        {
            var old = list[iIndexToMove + 1];
            list[iIndexToMove + 1] = list[iIndexToMove];
            list[iIndexToMove] = old;
        }
    }

    public static void Swap(this IList list, int firstIndex, int secondIndex)
    {
        if (list != null && firstIndex >= 0 &&
            firstIndex < list.Count && secondIndex >= 0 &&
            secondIndex < list.Count)
        {

            if (firstIndex == secondIndex)
            {
                return;
            }
            var temp = list[firstIndex];
            list[firstIndex] = list[secondIndex];
            list[secondIndex] = temp;
        }
    }

    public static int FindSum<TKey>(this IDictionary<TKey, int> dict)
    {
        var sum = 0;

        foreach (var d in dict)
        {
            sum += d.Value;
        }

        return sum;
    }

	public static string ToString(this IDictionary source, string separator)
	{
		if (source == null)
			throw new ArgumentNullException("source");
		
		if (source.Count == 0)
		{
			return "{}";
		}
		
		var sb = new StringBuilder("{");
		foreach (DictionaryEntry x in source)
		{
			sb.Append(x.Key).Append(separator);
			var valueAsDictionary = x.Value as IDictionary;
			if (valueAsDictionary != null)
			{
				sb.Append(valueAsDictionary.ToString(separator));
			}
			else
			{
				sb.Append(x.Value);
			}
			sb.Append(',');
		}
		sb[sb.Length - 1] = '}';
		return sb.ToString();
	}
	
	public static void Shuffle<T>(this IList<T> list)
	{
		var n = list.Count;
		while (n > 1)
		{
			n--;
			var k = UnityEngine.Random.Range(0, n + 1);
			var value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

    static public TValue TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key)
    {
        TValue result;
        dict.TryGetValue(key, out result);
        return result;
    }

    static public TValue TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue def)
    {
        TValue result;
        if (dict.TryGetValue(key, out result))
            return result;
        else
            return def;
    }

    static public Dictionary<TKey, TDest> ConvertValues<TKey, TSource, TDest>(this IDictionary<TKey, TSource> dict, System.Func<TSource, TDest> convertor)
    {
        var dictionary = new Dictionary<TKey, TDest>();
        
        foreach (var pair in dict)
            dictionary.Add(pair.Key, convertor(pair.Value));
        return dictionary;
    }

    static public List<TValue> FindValues<TKey, TValue>(this IDictionary<TKey, TValue> dict, Predicate<TValue> predicate)
    {
        var selected = new List<TValue>();
        var list = dict.Values;
        
        foreach (var value in list)
        {
            if (predicate(value))
            {
                selected.Add(value);
            }
        }
        return selected;
    }

    public static bool ContainsType<TType, TSource> (this List<TSource> source) where TType : class
	{
		for (int index = 0; index < source.Count; index++)
		{
			var source1 = source[index];
			if(source1 is TType)
				return true;
		}

		return false;
	}

    public static void AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value) where TKey : class
    {
        if (key == null)
        {
            return;
        }

        if (dictionary.ContainsKey(key))
        {
            dictionary[key] = value;
        }
        else
        {
            dictionary.Add(key, value);
        }
    }

    public static List<TResult> CreateAll<TSource, TResult>(this List<TSource> source, Func<TSource, TResult> createFunc)
    {
        var result = new List<TResult>(source.Count);
        for (int index = 0; index < source.Count; index++)
        {
            var source1 = source[index];
            result.Add(createFunc(source1));
        }
        return result;
    }
    public static TResult[] CreateAllAsArray<TSource, TResult>(this List<TSource> source, Func<TSource, TResult> createFunc)
    {
        var result = new TResult[source.Count];
        var index = 0;
        for (int i = 0; i < source.Count; i++)
        {
            var source1 = source[i];
            result[index++] = createFunc(source1);
        }
        return result;
    }

    public static IEnumerable<TResult> CastAll<TResult>(this IEnumerable source)
    {
        return source.Cast<TResult>();
    }

    public static TResult FindFirst<TResult>(this List<TResult> source) where TResult : class
    {
        if (source == null || source.Count == 0)
        {
            return null;
        }
        return source[0];
    }


    public static TResult FindLast<TResult>(this List<TResult> source) where TResult : class
    {
        if (source == null || source.Count == 0)
        {
            return null;
        }
        return source[source.Count - 1];
    }

    public static TResult FindFirst<TResult>(this TResult[] source) where TResult : class
    {
        if (source == null || source.Length == 0)
        {
            return null;
        }
        return source[0];
    }


    public static TResult FindLast<TResult>(this TResult[] source) where TResult : class
    {
        if (source == null || source.Length == 0)
        {
            return null;
        }
        return source[source.Length - 1];
    }

    public static bool FindAny<TSource>(this List<TSource> source, Func<TSource, bool> predicate)
    {
        if (source == null)
            return false;
        if (predicate == null)
            return false;
        for (int index = 0; index < source.Count; index++)
        {
            TSource source1 = source[index];
            if (predicate(source1))
            {
                return true;
            }
        }
        return false;
    }


    public static bool FindAny<TSource>(this TSource[] source, Func<TSource, bool> predicate)
    {
        if (source == null)
            return false;
        if (predicate == null)
            return false;
        for (int index = 0; index < source.Length; index++)
        {
            TSource source1 = source[index];
            if (predicate(source1))
            {
                return true;
            }
        }
        return false;
    }

    public static bool IsNullOrEmpty(this ICollection collection)
    {
        return collection == null || collection.Count == 0;
    }


    public static bool IsAll<T>(this List<T> source, Predicate<T> match)
    {
        if (source == null)
        {
            return false;
        }
        if (match == null)
        {
            return false;
        }

        foreach (var source1 in source)
        {
            if (!match(source1))
            {
                return false;
            }
        }

        return true;
    }

    public static int FindMax(this List<int> list)
    {
        int max = 0;
        for (int index = 0; index < list.Count; index++)
        {
            var i = list[index];
            if (i > max)
            {
                max = i;
            }
        }
        return max;
    }
    public static float FindMax(this List<float> list)
    {
        float max = 0;
        for (int index = 0; index < list.Count; index++)
        {
            var i = list[index];
            if (i > max)
            {
                max = i;
            }
        }
        return max;
    }

    public static TSource Random<TSource>(this List<TSource> list)
    {
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    public static T FindComponent<T>(this List<GameObject> items, Predicate<T> match) where T : Component
    {
        var components = new List<T>();
        for (int index = 0; index < items.Count; index++)
        {
            var item = items[index];
            var comp = item.GetComponent<T>();
            if (comp != null)
            {
                components.Add(comp);
            }
        }

        return components.Find(match);
    }
    public static int FindMax<TSource>(this IEnumerable<TSource> list, Func<TSource, int> predicate)
    {
        var max = 0;

        foreach (var item in list)
        {
            max = Mathf.Max(max, predicate(item));
        }

        return max;
    }

    public static int FindMax<TSource>(this List<TSource> list, Func<TSource, int> predicate)
    {
        var max = 0;

        for (int index = 0; index < list.Count; index++)
        {
            var item = list[index];
            max = Mathf.Max(max, predicate(item));
        }

        return max;
    }

    public static TSource FindMax<TSource>(this List<TSource> list, Func<TSource, TSource, bool> predicate) where TSource : class
    {
        TSource max = null;

        for (int index = 0; index < list.Count; index++)
        {
            var i = list[index];
            if (predicate(i, max))
            {
                max = i;
            }
        }

        return max;
    }

    public static int FindSum(this List<int> list)
    {
        var sum = 0;

        for (int index = 0; index < list.Count; index++)
        {
            sum += list[index];
        }

        return sum;
    }
    public static int FindSum<TSource>(this IEnumerable<TSource> list, Func<TSource, int> predicate)
    {
        var sum = 0;

        foreach (var item in list)
        {
            sum += predicate(item);
        }

        return sum;
    }

    public static int ItemCount<TSource>(this List<TSource> collection) where TSource : class
    {
        var count = 0;
        for (int index = 0; index < collection.Count; index++)
        {
            var source = collection[index];
            var i = source;
            if (i != null)
            {
                count++;
            }
        }
        return count;
    }
    public static int CountAll<TSource>(this List<TSource> collection, Predicate<TSource> match) where TSource : class
    {
        var count = 0;
        for (int index = 0; index < collection.Count; index++)
        {
            var source = collection[index];
            var i = source;
            if (match != null && match(i))
            {
                count++;
            }
        }
        return count;
    }
    public static List<TResult> CreateList<TKey, TSource, TResult>(this Dictionary<TKey, TSource> list, Func<TSource, TResult> createFunc) where TResult : class
    {
        var result = new List<TResult>(list.Count);
        foreach (var source in list)
        {
            var create = createFunc(source.Value);
            if (create != null)
            {
                result.Add(create);
            }
        }
        return result;
    }

    public static List<TResult> CreateList<TSource, TResult>(this List<TSource> list, Func<TSource, TResult> createFunc)
    {
        var result = new List<TResult>(list.Count);
        for (int index = 0; index < list.Count; index++)
        {
            result.Add(createFunc(list[index]));
        }
        return result;
    }

    public static List<TResult> CreateList<TSource, TResult>(this TSource[] array, Func<TSource, TResult> createFunc)
    {
        var result = new List<TResult>(array.Length);
        for (int index = 0; index < array.Length; index++)
        {
            result.Add(createFunc(array[index]));
        }
        return result;
    }

    public static List<TResult> CreateList<TSourceKey, TSourceValue, TResult>(this Dictionary<TSourceKey, TSourceValue>.ValueCollection collection, Func<TSourceValue, TResult> createFunc)
    {
        var result = new List<TResult>(collection.Count);
        foreach (var obj in collection)
        {
            result.Add(createFunc(obj));
        }
        return result;
    }

    public static TResult[] CreateArray<TSource, TResult>(this List<TSource> list, Func<TSource, TResult> createFunc)
    {
        var result = new TResult[list.Count];
        for (int index = 0; index < list.Count; index++)
        {
            result[index] = createFunc(list[index]);
        }
        return result;
    }

    public static TResult[] CreateArray<TSource, TResult>(this TSource[] array, Func<TSource, TResult> createFunc)
    {
        var result = new TResult[array.Length];
        for (int index = 0; index < array.Length; index++)
        {
            result[index] = createFunc(array[index]);
        }
        return result;
    }

    public static TResult[] CreateArray<TSourceKey, TSourceValue, TResult>(this Dictionary<TSourceKey, TSourceValue>.ValueCollection collection, Func<TSourceValue, TResult> createFunc)
    {
        var result = new TResult[collection.Count];
        var index = 0;
        foreach (var obj in collection)
        {
            result[index++] = createFunc(obj);
        }
        return result;
    }
    public static TResult[] CreateArray<TSourceKey, TSourceValue, TResult>(this Dictionary<TSourceKey, TSourceValue>.KeyCollection collection, Func<TSourceKey, TResult> createFunc)
    {
        var result = new TResult[collection.Count];
        var index = 0;
        foreach (var obj in collection)
        {
            result[index++] = createFunc(obj);
        }
        return result;
    }
    public static TSourceKey[] CreateArray<TSourceKey, TSourceValue>(this Dictionary<TSourceKey, TSourceValue>.KeyCollection collection)
    {
        var result = new TSourceKey[collection.Count];
        var index = 0;
        foreach (var obj in collection)
        {
            result[index++] = obj;
        }
        return result;
    }

    public static List<TSource> CreateList<TSource>(this IEnumerable<TSource> source)
    {
        return source.ToList();
    }

    public static TSource FindFirstOrDefault<TSource>(this List<TSource> source, Func<TSource, bool> predicate) where TSource : class
    {
        if (source == null)
            return null;
        if (predicate == null)
        {
            if (source.Count > 0)
            {
                return source[0];
            }
            return null;
        }
        for (int index = 0; index < source.Count; index++)
        {
            TSource source1 = source[index];
            if (predicate(source1))
            {
                return source1;
            }
        }
        return null;
    }

    public static TSource FindFirstOrDefault<TSource>(this TSource[] source, Func<TSource, bool> predicate) where TSource : class
    {
        if (source == null)
            return null;
        if (predicate == null)
        {
            if (source.Length > 0)
            {
                return source[0];
            }
            return null;
        }
        for (int index = 0; index < source.Length; index++)
        {
            TSource source1 = source[index];
            if (predicate(source1))
            {
                return source1;
            }
        }
        return null;
    }
    public static TSource FindFirstOrDefault<TKey, TSource>(this Dictionary<TKey, TSource> source, Func<KeyValuePair<TKey, TSource>, bool> predicate) where TSource : class
    {
        if (source == null)
            return null;
        if (predicate == null)
        {
            if (source.Count > 0)
            {
                return source.First().Value;
            }
            return null;
        }
        foreach (var source1 in source)
        {
            if (predicate(source1))
            {
                return source1.Value;
            }
        }
        return null;
    }
}