using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TimeExtension
{
    public static bool IsAtLeastYesterday(this DateTime time)
    {
        return DateTime.Now - time >= TimeSpan.FromDays(1);
    }
    public static bool IsFuture(this DateTime time)
    {
        return time - DateTime.Now >= TimeSpan.FromTicks(1);
    }
}
