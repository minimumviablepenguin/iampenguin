using System;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public static class TransformExtensions
{
    public static Transform FindParent(this Transform gObj, string name)
    {
        return ParentFindWithName(gObj, ref name);
    }

    private static Transform ParentFindWithName(Transform gObj, ref string name)
    {
        Transform parentTransform = gObj.transform.parent;

        if (parentTransform != null)
        {
            if(parentTransform.gameObject.name != name)
            {
                return ParentFindWithName(parentTransform, ref name);
            }
            else
            {
                return parentTransform;
            }
        }

        return null;
    }

    /// <summary>
    /// Similar function to GetComponentsInParent in 4.6, but this does not check the current gameObject.
    /// </summary>
    /// <param name="gObj"></param>
    /// <param name="type"></param>
    /// <param name="includeInactive"></param>
    /// <param name="container"></param>
    /// <returns></returns>
    public static Component[] GetComponentsInParent<T>(this GameObject gObj, bool includeInactive = false)
    {
        if (!typeof(T).IsSubclassOf(typeof(Component)))
            throw new System.InvalidOperationException("typeof " + typeof(T).ToString() + " is not a derived type of UnityEngine.Component.");

        List<Component> components = new List<Component>();

        components = GetComponentsInParent(gObj, typeof(T), includeInactive, components);

        Debug.Log("Found " + components.Count + " components of " + typeof(T).ToString() + " in parent");

        return components.ToArray<Component>();
    }

    private static List<Component> GetComponentsInParent(GameObject gObj, Type type, bool includeInactive, List<Component> container)
    {
        Transform parentTransform = gObj.transform.parent;

        if (parentTransform != null)
        {
            if (!(includeInactive == false && parentTransform.gameObject.activeSelf == false))
            {
                Component component = parentTransform.gameObject.GetComponent(type) as Component;

                if (component != null)
                    container.Add(component);
            }

            return GetComponentsInParent(parentTransform.gameObject, type, includeInactive, container);
        }

        return container;
    }

    /// <summary>
    /// Similar function to GetComponentInParent in 4.6, but this does not check the current gameObject.
    /// </summary>
    /// <param name="gObj"></param>
    /// <param name="type"></param>
    /// <param name="includeInactive"></param>
    /// <param name="container"></param>
    /// <returns></returns>
    public static Component GetComponentInParent<T>(this GameObject gObj, bool includeInactive = false)
    {
        if (!typeof(T).IsSubclassOf(typeof(Component)))
            throw new System.InvalidOperationException("typeof " + typeof(T).ToString() + " is not a derived type of UnityEngine.Component.");

        return GetComponentInParent(gObj, typeof(T), includeInactive);
    }

    private static Component GetComponentInParent(GameObject gObj, Type type, bool includeInactive)
    {
        Transform parentTransform = gObj.transform.parent;

        if (parentTransform != null)
        {
            if (!(includeInactive == false && parentTransform.gameObject.activeSelf == false))
            {
                Component component = parentTransform.gameObject.GetComponent(type) as Component;

                if (component != null)
                    return component;
            }

            return GetComponentInParent(parentTransform.gameObject, type, includeInactive);
        }

        return null;
    }
}