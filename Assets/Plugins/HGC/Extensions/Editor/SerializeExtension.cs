﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;

public static class SerializeExtension {

    public static Type GetUnderlyingType(this SerializedProperty prop)
    {
        switch (prop.propertyType)
        {
            case SerializedPropertyType.Integer:
                return typeof(int);
            case SerializedPropertyType.Float:
                return typeof(float);
            case SerializedPropertyType.Boolean:
                return typeof(bool);
            case SerializedPropertyType.String:
                return typeof(string);
            case SerializedPropertyType.Color:
                return typeof(Color);
            case SerializedPropertyType.Bounds:
                return typeof(Bounds);
            case SerializedPropertyType.Rect:
                return typeof(Rect);
            case SerializedPropertyType.Vector2:
                return typeof(Vector2);
            case SerializedPropertyType.Vector3:
                return typeof(Vector3);
            case SerializedPropertyType.ObjectReference:
                return prop.objectReferenceValue.GetType();
            default: throw new Exception("Invalid type: " + prop.propertyType.ToString());
        }
    }
}
