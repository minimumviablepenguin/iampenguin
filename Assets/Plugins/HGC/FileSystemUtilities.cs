using System.Collections.Generic;
using UnityEngine;

namespace HGC
{
    public class FileSystemUtilities
    {
        public static readonly string[] IgnoreFolderNames = { "Ignore", "Test" };

        public static string FileToAssetPath(string filePath)
        {
            if (filePath.StartsWith("Assets"))
            {
                Log.Warn("FileSystemUtilities", "Filepath was actually an AssetPath. Conversion was unneccessary.");
                return filePath;
            }

            return "Assets" + filePath.Replace(Application.dataPath, "");
        }

        public static string AssetToFilePath(string assetPath)
        {
            if(!assetPath.StartsWith("Assets/"))
            {
                Log.Error("Utils", "Path is not an asset path. It does not start with 'Assets/'");
                return assetPath;
            }

            return Application.dataPath + assetPath.Substring(assetPath.IndexOf("/"));
        }

        private static string AppendMetaExtension(string dirPath)
        {
            return dirPath + ".meta";
        }

        public static bool IsMetaFile(string path)
        {
            return path.EndsWith(".meta");
        }

        public static bool IsInIgnoredFolder(EditorPath editorPath)
        {
            return IsInIgnoredFolder(editorPath.PostProcessPath);
        }

        public static bool IsInIgnoredFolder(string path)
        {
            int fSlashIndex = path.LastIndexOf("/");
            int bSlashIndex = path.LastIndexOf("\\");
            int endOfFolderPart = (fSlashIndex > bSlashIndex) ? fSlashIndex : bSlashIndex;

            bool isInIgnoreFolder = false;
            foreach (string ignoreStr in IgnoreFolderNames)
            {
                // Skip paths in an IGNORE folder, or a folder marked with the IGNORE characters
                if (path.Substring(0, endOfFolderPart).ToLowerInvariant().Contains(ignoreStr.ToLowerInvariant()))
                {
                    isInIgnoreFolder = true;
                    break;
                }
            }

            return isInIgnoreFolder;
        }

        public static List<string> FindFilesInDirectoriesNotInc(string folderPath, string notIncExtension, System.IO.SearchOption depth = System.IO.SearchOption.AllDirectories)
        {
            List<string> matchingFilePaths = new List<string>();

            if (!System.IO.Directory.Exists(folderPath))
                return matchingFilePaths;

#if !UNITY_WEBPLAYER
            string[] filePaths = System.IO.Directory.GetFiles(folderPath, "*.*", depth);

            foreach (string path in filePaths)
            {
                if (!IsInIgnoredFolder(path))
                {
                    if (!path.ToLower().EndsWith(notIncExtension))
                    {
                        matchingFilePaths.Add(path.Replace("\\", "/"));
                    }
                }
            }
#else
            Log.Warn("Debug", "System.IO.Directory.GetFiles() not supported on Web Player.");
#endif
            return matchingFilePaths;
        }

        public static List<string> FindFilesInDirectories(string folderPath, string extension, bool includeIgnoreFolders = false, System.IO.SearchOption depth = System.IO.SearchOption.AllDirectories)
		{
			List<string> matchingFilePaths = new List<string>();

			if(!System.IO.Directory.Exists(folderPath))
				return matchingFilePaths;

#if !UNITY_WEBPLAYER
            string[] filePaths = System.IO.Directory.GetFiles(folderPath, "*.*", depth);

			foreach(string path in filePaths)
			{
                if (includeIgnoreFolders || !IsInIgnoredFolder(path))
                {
                    if (path.ToLower().EndsWith(extension))
                    {
                        matchingFilePaths.Add(path.Replace("\\", "/"));
                    }
                }
			}
#else
            Log.Warn("Debug", "System.IO.Directory.GetFiles() not supported on Web Player.");
#endif
            return matchingFilePaths;
		}

		public static string FindFileInDirectories(string name, string folderPath, string extension, bool includeIgnoreFolders = false, System.IO.SearchOption depth = System.IO.SearchOption.AllDirectories)
		{
			string pathMatchingName = string.Empty;
			string file = name + "." + extension;

			if(!System.IO.Directory.Exists(folderPath))
				return null;

#if !UNITY_WEBPLAYER
			string[] filePaths = System.IO.Directory.GetFiles(folderPath, "*.*", depth);

			foreach(string path in filePaths)
			{
				int fSlashIndex = path.LastIndexOf("/");
				int bSlashIndex = path.LastIndexOf("\\");
				int endOfFolderPart = (fSlashIndex > bSlashIndex) ? fSlashIndex : bSlashIndex;

                if (!includeIgnoreFolders)
                {
                    bool isInIgnoreFolder = false;
                    foreach (string ignoreStr in IgnoreFolderNames)
                    {
                        // Skip paths in an IGNORE folder, or a folder marked with the IGNORE characters
                        if (path.Substring(0, endOfFolderPart).ToLowerInvariant().Contains(ignoreStr.ToLowerInvariant()))
                        {
                            isInIgnoreFolder = true;
                            break;
                        }
                    }

                    if (isInIgnoreFolder)
                        continue;
                }

				if(path.ToLower().EndsWith(file))
				{
					pathMatchingName = path.Replace("\\", "/");
					break;
				}
			}
#else
            Log.Warn("Debug", "System.IO.Directory.GetFiles() not supported on Web Player.");
#endif

            return pathMatchingName;
		}

#region UnityEditorMethods

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void RemoveMacDirectoryFiles()
        {
#if UNITY_EDITOR
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_STANDALONE_LINUX || UNITY_IOS
            Log.Info("Utils", "You are on a Linux/Mac setup. Aborting the deletion of your directory files...");
            return;
#endif
            var badPlatformFiles = FindFilesInDirectories(Application.dataPath, ".DS_Store", true);
            int total = badPlatformFiles.Count;

            if (total == 0)
            {
                Log.Info("Utils", "Cleaned '" + total + "' Mac Directory Files");
                return;
            }

            UnityEditor.EditorUtility.DisplayCancelableProgressBar("Delete Mac Directory Files", "Finding empty folders", 0);
            for (int index = 0; index < total; index++)
            {
                string path = badPlatformFiles[index];

                System.IO.Directory.Delete(path);

                string metaFilepath = path + ".meta";

                if (System.IO.File.Exists(metaFilepath))
                    System.IO.File.Delete(metaFilepath);

                var cancel = UnityEditor.EditorUtility.DisplayCancelableProgressBar("Delete Mac Directory Files", "Deleting " + path, index / (float)total);
                if (cancel)
                    break;
            }

            Log.Info("Utils", "Cleaned '" + total + "' Mac Directory Files");
            UnityEditor.EditorUtility.ClearProgressBar();
#endif
        }

#endregion
    }
}
