﻿using UnityEngine;
using System.Collections;
using HGC.Events;

public static class MessengerExtensions
{
    public static void Dispatch<TMessage>(this GameObject gameObject, string groupName, params System.Object[] args) where TMessage : class, IMessageDispatcher, new()
    {
        MessageManager.Dispatch<TMessage>(groupName, args);
    }

    public static void Broadcast<TMessage>(this GameObject gameObject, params System.Object[] args) where TMessage : class, IMessageDispatcher, new()
    {
        MessageManager.Broadcast<TMessage>(args);
    }

#region AddOthersToDispatch
    public static void AddDispatchGroup(this GameObject gameObject, string parentGroupName, string groupName, params GameObject[] gameObjects)
    {
        MessageManager.Instance[parentGroupName].AddDispatchGroup(groupName, gameObjects);
    }

    public static void AddGlobalDispatchGroup(this GameObject behaviour, string groupName, params GameObject[] gameObjects)
    {
        MessageManager.Instance.AddDispatchGroup(groupName, gameObjects);
    }

    public static void AddToAllDispatchGroups(this GameObject gameObject, params GameObject[] gameObjects) 
    {
        MessageManager.Instance.AddToDispatchGroup(gameObjects);
    }
#endregion

#region SelfToDispatch
    public static void AddSelfToDispatchGroup(this GameObject gameObject, string parentGroupName, string groupName)
    {
        MessageManager.Instance[parentGroupName].AddDispatchGroup(groupName, gameObject);
    }

    public static void AddSelfToGlobalDispatchGroup(this GameObject gameObject, string groupName)
    {
        MessageManager.Instance.AddDispatchGroup(groupName, gameObject);
    }

    public static void AddSelfToAllDispatchGroups(this GameObject gameObject)
    {
        MessageManager.Instance.AddToDispatchGroup(gameObject);
    }
#endregion
}
