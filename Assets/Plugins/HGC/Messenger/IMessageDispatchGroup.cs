﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace HGC.Events
{
    public interface IMessageDispatchGroup
    {
        string GroupID { get; }
        IMessageDispatchGroup Parent { get; }

        IEnumerable<GameObject> GameObjects { get; }
        IEnumerable<IMessageDispatchGroup> ChildGroups { get; }

        IMessageDispatchGroup AddToDispatchGroup(params GameObject[] gameObjects);
        IMessageDispatchGroup AddDispatchGroup(string name, params GameObject[] gameObjects);         
    }
}
