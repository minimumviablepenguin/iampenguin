﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace HGC.Events
{
    /// <summary>
    /// Use this to override and handle internals of the event firing
    /// </summary>
    public class InternalMessageData : BaseEventData
    {
        //public System.Object[] Data { get; private set; }

        public InternalMessageData(//params System.Object[] data
            ) : base(EventSystem.current)
        {
            //Data = data;

            // send message here

            // does this need to be set? benefits?
            //base.selectedObject;
        }
    }
}
