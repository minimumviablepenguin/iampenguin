﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HGC.Events
{
    public interface IMessageDispatcher
    {
        void Dispatch(IMessageDispatchGroup group, params System.Object[] args);
    }
}
