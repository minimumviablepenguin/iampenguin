﻿using System.Collections.Generic;
using UnityEngine;

namespace HGC.Events
{
    public class MessageDispatchGroup : IMessageDispatchGroup
    {
        private List<GameObject> _gameObjects;
        private List<IMessageDispatchGroup> _childGroups;

        public MessageDispatchGroup(IMessageDispatchGroup owner, string groupName)
        {
            _gameObjects = new List<GameObject>();
            _childGroups = new List<IMessageDispatchGroup>();

            GroupID = groupName;
            Parent = owner;
        }

        public IMessageDispatchGroup Parent
        {
            get;
            private set;
        }

        public string GroupID
        {
            get;
            private set;
        }

        public IEnumerable<IMessageDispatchGroup> ChildGroups
        {
            get
            {
                return _childGroups;
            }
        }

        public IEnumerable<GameObject> GameObjects
        {
            get
            {
                return _gameObjects;
            }
        }

        public static MessageDispatchGroup operator +(MessageDispatchGroup thisGroup, GameObject thatGameObject)
        {
            thisGroup.AddDispatchGroup(thisGroup.GroupID, thatGameObject);

            return thisGroup;
        }


        public IMessageDispatchGroup AddToDispatchGroup(params GameObject[] gameObjects)
        {
            foreach (var gameObject in gameObjects)
            {
                if (!_gameObjects.Contains(gameObject))
                {
                    _gameObjects.Add(gameObject);
                }
            }

            return this;
        }

        public IMessageDispatchGroup AddDispatchGroup(string name, params GameObject[] gameObjects)
        {
            IMessageDispatchGroup group = MessageManager.Instance[name];

            if (group != null)
            {
                if (!System.Object.ReferenceEquals(group.Parent, this))
                {
                    Log.Error("Events", "Tried to add event dispatch group but it was already existed on a different parent. Event groups must be globally unique: " + name);
                    return group;
                }
            }
            else
            {
                group = new MessageDispatchGroup(Parent, name);
                _childGroups.Add(group);

                // bubble the dispatch group through all of the parents, so we can invocate at each level
                Parent.AddDispatchGroup(name, gameObjects);
            }

            foreach (var gameObj in gameObjects)
            {
                if(!_gameObjects.Contains(gameObj))
                    _gameObjects.Add(gameObj);
            }       

            return group;
        }
    }
}
