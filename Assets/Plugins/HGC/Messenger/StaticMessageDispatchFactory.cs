﻿using System;
using System.Collections.Generic;

namespace HGC.Events
{
    public class StaticMessageDispatchFactory
    {
        private static Dictionary<System.Type, IMessageDispatcher> _staticDispatchers = new Dictionary<Type, IMessageDispatcher>();

        public static IMessageDispatcher GetDispatcher<TDispatcher>(System.Type type) where TDispatcher : class, IMessageDispatcher, new()
        {
            IMessageDispatcher staticDispatcher;
            if (_staticDispatchers.TryGetValue(type, out staticDispatcher))
            {
                // found a cached static dispatcher
                return staticDispatcher;
            }
            else
            {
                // none existed yet - create, cache and return
                return _staticDispatchers[type] = new TDispatcher();
            }
        }
    }
}
