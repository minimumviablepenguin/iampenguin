﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using HGC;
using UnityEngine;

namespace HGC.Events
{
    public class MessageManager : Singleton<MessageManager>, IMessageDispatchGroup
    {
        private Dictionary<Type, IMessageDispatcher> _dispatchers;

        private Dictionary<string, IMessageDispatchGroup> _allGroups;
        private List<GameObject> _gameObjects;
        private List<IMessageDispatchGroup> _childGroups;

        private List<string> _messageNames;
        private List<Type> _messageTypes;

        public const string RootGroupName = "root"; 

        public override void OnInitialise()
        {          
            _dispatchers = new Dictionary<Type, IMessageDispatcher>();
            _allGroups = new Dictionary<string, IMessageDispatchGroup>();
            _gameObjects = new List<GameObject>();
            _childGroups = new List<IMessageDispatchGroup>();
            GroupID = RootGroupName;

            _allGroups.Add(GroupID, this);
            Log.Info("Singleton", "Initialised Message Manager");
        }

        public List<string> MessageNames
        {
            get
            {
                if (_messageNames != null)
                    return _messageNames;

                _messageNames = new List<string>();
                var keys = _dispatchers.Keys.ToList();

                for (int i = 0; i < keys.Count; i++)
                    _messageNames.Add(keys[i].ToString());

                return _messageNames;
            }
        }

        public List<Type> MessageTypes
        {
            get
            {
                if (_messageTypes != null)
                    return _messageTypes;

                _messageTypes = _dispatchers.Keys.ToList();

                return _messageTypes;
            }
        }

        public List<IMessageDispatcher> GetDispatchers()
        {
            if (!ValidateInstance())
            {
                return new List<IMessageDispatcher>();
            }
                
            return _dispatchers.Values.ToList();
        }

#region StaticMethods
        public static void Broadcast<TMessage>(params System.Object[] args) where TMessage : class, IMessageDispatcher, new()
        {
            Dispatch<TMessage>(RootGroupName, args);
        }

        public static void Dispatch<TMessage>(string groupName, params System.Object[] args) where TMessage : class, IMessageDispatcher, new()
        {
            if (!ValidateInstance())
                return;

            Instance.WarnIfBadLookupChars(groupName);

            if (Instance._allGroups.ContainsKey(groupName))
            {
                Instance.DispatchInternal<TMessage>(Instance._allGroups[groupName], args);
            }
            else
            {
                Log.Error("Messenger", "Group does not exist for: " + groupName);
            }
        }
#endregion

        private void DispatchInternal<TMessage>(IMessageDispatchGroup group = null, params System.Object[] args) where TMessage : class, IMessageDispatcher, new()
        {
            if (group == null)
                group = this;

            System.Type type = typeof(TMessage);

            if (!ValidateInstance())
                return;

            if (!_dispatchers.ContainsKey(type))
            {
                var staticDispatcher = StaticMessageDispatchFactory.GetDispatcher<TMessage>(type);

                if (staticDispatcher != null)
                {
                    _dispatchers.Add(type, staticDispatcher);
                }
                else
                {
                    Log.Error("Messenger", "MessageManager does not contain a dispatcher for the type: " + type.ToString());
                    return;
                }
            }

            TMessage dispatcher = _dispatchers[type] as TMessage;

            Log.Info("MESSENGER", "dispatching event message: " + type);
            dispatcher.Dispatch(group, args);
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public void WarnIfBadLookupChars(string chars)
        {
#if UNITY_EDITOR
            if (string.IsNullOrEmpty(name))
            {
                Log.Error("Messages", "Lookup character string was null!");
                return;
            }

            if (chars.Any(c => char.IsUpper(c)))
                Log.Error("Messenger", "Lookup characters contained upper-cade characters: '" + chars + "'. All strings should all be lower-case, to avoid naming mistakes.");
#endif
        }

        public IMessageDispatchGroup this[string name]
        {
            get
            {
                if (!ValidateInstance())
                    return null;

                WarnIfBadLookupChars(name);

                if(_allGroups.ContainsKey(name))
                {
                    return _allGroups[name] as IMessageDispatchGroup;
                }
                else
                {
                    Log.Error("Messenger", "Indexer did not have a MessageDispatchGroup named '" + name + "'. Available groups are: " + string.Join(", ", _allGroups.Keys.ToArray()));
                    return null;
                }
            }
        }

        #region IEventGroup

        public IMessageDispatchGroup AddToDispatchGroup(params GameObject[] gameObjects)
        {
            foreach (var gameObject in gameObjects)
            {
                if (!_gameObjects.Contains(gameObject))
                {
                    Log.Info("Messenger", "Adding '" + gameObject.name + "' gameobject to dispatch group: " + GroupID);

                    _gameObjects.Add(gameObject);
                }
            }

            return this;
        }

        public IMessageDispatchGroup AddDispatchGroup(string name, params GameObject[] gameObjects)
        {
            if (!ValidateInstance())
                return null;

            WarnIfBadLookupChars(name);

            MessageDispatchGroup group;

            if (_allGroups.ContainsKey(name))
            {
                group = _allGroups[name] as MessageDispatchGroup;

                if (!System.Object.ReferenceEquals(group.Parent, this))
                {
                    Log.Error("Events", "Tried to add dispatch group but it already existed on a different parent. EventDispatch groups must be unique: " + name, gameObject);
                    return group;
                }

                Log.Info("Messenger", "Adding '" + gameObject.name + "' gameobject to dispatch group: " + GroupID);
                group.AddToDispatchGroup(gameObjects);  
            }
            else
            {
                group = new MessageDispatchGroup(this, name);
                _allGroups.AddOrUpdate(name, group);
                _childGroups.Add(group);

                group.AddToDispatchGroup(gameObjects);
            }

            return group;
        }

        public IMessageDispatchGroup Parent
        {
            get;
            private set;
        }

        public string GroupID
        {
            get;
            private set;
        }

        public IEnumerable<IMessageDispatchGroup> ChildGroups
        {
            get
            {
                return _childGroups;
            }
        }

        public IEnumerable<GameObject> GameObjects
        {
            get
            {
                return _gameObjects;
            }
        }

#endregion IEventGroup;
    }
}
