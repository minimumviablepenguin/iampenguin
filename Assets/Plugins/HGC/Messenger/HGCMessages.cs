﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine;


namespace HGC.Events
{
    /*
    These message types are just for the HGC codebase, Krillbite messages should not be stored in the HGC codebase
    */

#region HGCMessages

    /// Game Loaded ////////////////////////////
    
    public interface IGameLoadedHandler : IEventSystemHandler
    {
        void GameLoaded();
    }

    public class GameLoaded : BaseMessageDispatcher
    {
        protected override void DispatchInternal(GameObject target, InternalMessageData eventData, params System.Object[] args)
        {
            // this is sent to all game objects
            //Log.Info("Messenger", "Dispatch internal message event to target '" + target.name + "' - (On Game Loaded)", target);

            ExecuteEvents.Execute(target, eventData, (IGameLoadedHandler handler, BaseEventData data) => handler.GameLoaded());
        }
    }

    /// Dummy Example ////////////////////////////

    public interface ICustomMessageHandler : IEventSystemHandler
    {
        void CustomMessage(float fantasticFloat, int indomitableInteger, string sassyString);
    }

    public class CustomMessage : BaseMessageDispatcher
    {
        protected override void DispatchInternal(GameObject target, InternalMessageData eventData, params System.Object[] args)
        {
            Log.Info("Messenger", "Dispatch internal message event to target '" + target.name + "' - (" + GetType().Name + ")", target);

            ExecuteEvents.Execute(target, eventData, (ICustomMessageHandler handler, BaseEventData data) => handler.CustomMessage((float)args[0], (int)args[1], (string)args[2]));
        }
    }

    #endregion
}
