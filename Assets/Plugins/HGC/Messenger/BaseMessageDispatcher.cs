﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace HGC.Events
{
    public class BaseMessageDispatcher : IMessageDispatcher
    {
        public static InternalMessageData _staticMessageData;

        protected MessageManager Manager
        {
            get { return MessageManager.Instance; }
        }

        private InternalMessageData StaticMessageData
        {
            get
            {
                if (_staticMessageData != null)
                    return _staticMessageData;
                else
                    return _staticMessageData = new InternalMessageData();
            }
        }

        public void Dispatch(IMessageDispatchGroup group, params System.Object[] args)
        {
            if (group == null)
            {
                Log.Error("Messenger", "'MessageDispatchGroup' object was null.");
                return;
            }

            try
            {
                foreach (var target in group.GameObjects)
                {
                    DispatchInternal(target, StaticMessageData, args);
                }
            }
            catch
            {
                Log.Error("Messenger", "Failed to execute event message - event parameters were incorrect for message: " + GetType());
            }
        }

        protected virtual void DispatchInternal(GameObject target, InternalMessageData eventData, params System.Object[] args)
        {
            // This must be overriden, since its where we actually invoke the message event
            throw new NotImplementedException("The subclass of MessageDispatcher did not override the method: 'DispatchInternal'. This must be implement and execute the event with the correct parameters for the Custom Message: " + GetType());
        }
    }
}
