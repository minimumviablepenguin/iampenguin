﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Collections;

namespace HGC.Events
{
    [CustomEditor(typeof(MessageManager))]
    public class MessageManagerInspector : Editor
    {
        List<string> _dispatcherNames;
        List<IMessageDispatcher> _dispatchers;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var messenger = target as MessageManager;

            if (messenger == null || !messenger.IsInitialised())
                return;

            _dispatchers = messenger.GetDispatchers();
            if (_dispatchers.IsNullOrEmpty())
            {
                return;
            }
            else
            {
                _dispatcherNames = messenger.MessageNames;
            }


            // can use this to move between groups
            for (int i = 0; i < _dispatchers.Count; i++)
            {
                if (GUILayout.Button("Dispatch '" + _dispatcherNames[i] + "'"))
                {
                    // test instance
                    _dispatchers[i].Dispatch(MessageManager.Instance);
                    // test indexer
                    //_dispatchers[i].Dispatch(MessageManager.Instance[MessageManager.RootGroupName], 1f, 5, "wooo");
                }
            }
        }  
    }
}
