﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;
using HGC.Events;
using HGC.Utils;

namespace HGC
{
    public class ManagedBehaviour : MonoBehaviour, IManagedBehaviour, IManagedCoroutine
    // Custom Event Messages (comment these out to remove them)
    , IGameLoadedHandler
    {
        /*
        //[FormerlySerializedAs("myValue")]
        [TagGroup(AllowUntagged = true)]
        public string FindTag;

        //[FormerlySerializedAs("myValue")]
        [Scene(IncludeTestScenes = true)]
        public string FindScene;

        [Message]
        public string FindMessage;
        */

        #region StartupCallbacks
        /// <summary>
        /// Don't hide this method - Override 'OnAwake()' instead. Invoked when the monobehaviour is first instantiated.
        /// </summary>
        protected virtual void Awake()
        {
            OnAwake();
        }

        /// <summary>
        ///Invoked when the monobehaviour is first instantiated.
        /// </summary>
        protected virtual void OnAwake() { }

        /// <summary>
        /// Don't hide this method - Override 'OnStart()' instead. Invoked after EVERY monobehaviour is first instantiated.
        /// </summary>
        protected virtual void Start()
        {
            gameObject.AddSelfToAllDispatchGroups();
            CacheVariablesOnStart();
            OnStart();
        }

        /// <summary>
        /// Invoked after EVERY monobehaviour is first instantiated.
        /// </summary>
        protected virtual void OnStart() { }

        /// <summary>
        /// Don't hide this method - Override 'OnGameLoaded()' instead. Invoked after every singleton/manager or subscene has started.
        /// </summary>
        public virtual void GameLoaded()
        {
            //Log.InfoOnce("ManagedBehaviour", "ManagedBehaviour.GameLoaded", "OnGameLoaded");
            OnGameLoaded();
        }

        /// <summary>
        /// Invoked after every singleton/manager or subscene has started.
        /// </summary>
        protected virtual void OnGameLoaded() { }
        #endregion StartupCallbacks

#region EditorCallbacks
#if UNITY_EDITOR
        private void Reset()
        {
            Log.Info("Editor Reset on component " + name);
            OnReset();          
        }
#endif

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        protected virtual void OnReset()
        {

        }
#endregion EditorCallbacks

#region CachedVariables
        protected void CacheVariablesOnStart()
        {
            GameObject = gameObject;
            Transform = transform;
            _typeName = GetType().ToString();
        }

        [NonSerialized]
        private string _tag;
        public string Tag
        {
            get
            {
                if (string.IsNullOrEmpty(_tag))
                {
                    _tag = tag;
                }

                return _tag;
            }
            set
            {
                // TODO - move the gameobjects around in the event hierarchy
                _tag = value;
                tag = value;
            }
        }

        [NonSerialized]
        private string _typeName;
        public string TypeName
        {
            get
            {
                if (!string.IsNullOrEmpty(_typeName))
                    return _typeName;
                else
                    _typeName = GetType().ToString();

                return _typeName;
            }
        }

        [NonSerialized]
        private GameObject _gameObject;
        public GameObject GameObject
        {
            get
            {
                if (_gameObject == null)
                {
                    _gameObject = gameObject;
                }

                return _gameObject;
            }
            private set { _gameObject = value; }
        }

        [NonSerialized]
        private Transform _transform;
        public Transform Transform
        {
            get
            {
                if (_transform == null)
                {
                    _transform = transform;
                }

                return _transform;
            }
            private set { _transform = value; }
        }

        [NonSerialized]
        private RectTransform _rectTransform;
        public RectTransform RectTransform
        {
            get
            {
                if (_rectTransform == null)
                {
                    _rectTransform = transform as RectTransform;
                }

                return _rectTransform;
            }
            private set { _rectTransform = value; }
        }

        [NonSerialized]
        private string _name;
        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    _name = name;
                }

                return _name;
            }
            set
            {
                _name = value;
                name = value;
            }
        }
#endregion CachedVariables

#region IManagedCoroutine
        public const float GlobalTimeScale = 1f;
        private float _timeScale = 1f;

        public bool IsPaused { get; protected set; }
        public bool IsStopped { get; protected set; }

        protected virtual void OnPause()
        {
            Log.Info("ManagedBehaviour: paused!");
        }

        protected virtual void OnResume()
        {
            Log.Info("ManagedBehaviour: removed!");
        }

        protected float PausedDeltaTime
        {
            get
            {
                if (IsPaused)
                    return 0f;
                else
                    return Time.deltaTime * Mathf.Max(0f, _timeScale * GlobalTimeScale);
            }
        }

        public void PauseCoroutine()
        {
            Log.Assert(IsPaused, "ManagedBehaviour", "Coroutine is already paused");

            if (!IsPaused)
            {
                IsPaused = true;
                OnPause();
            }        
        }

        public void ResumeCoroutine()
        {
            Log.Assert(!IsPaused, "ManagedBehaviour", "Coroutine is was not paused. Could not resume");

            if (IsPaused)
            {
                IsPaused = false;
                OnResume();
            }
        }

        public Coroutine StartPausableCoroutine(IEnumerator coroutine)
        {
            return StartCoroutine(PausableCoroutine(coroutine));
        }

        public Coroutine PauseForSeconds(float seconds)
        {
            return StartPausableCoroutine(DoWaitForSeconds(seconds));
        }

        public Coroutine WaitForSeconds(float seconds, Func<bool> breakOut = null)
        {
            return StartCoroutine(DoWaitForSecondsInternal(seconds, breakOut));
        }

        public Coroutine AfterSeconds(float seconds, Action callback, Func<bool> breakOut = null)
        {
            return StartCoroutine(DoWaitForSecondsInternal(seconds, breakOut, callback));
        }

        /// private coroutine methods...

        private IEnumerator DoWaitForSeconds(float seconds)
        {
            while (seconds > 0)
            {
                seconds -= PausedDeltaTime;
                yield return null;
            }
        }

        private static IEnumerator DoWaitForSecondsInternal(float seconds, Func<bool> breakOut, Action callback = null)
        {
            while (seconds > 0)
            {
                seconds -= Time.deltaTime;
                if (breakOut != null && breakOut())
                {
                    break;
                }
                yield return null;
            }

            if (callback != null)
                callback();
        }

        private IEnumerator PausableCoroutine(IEnumerator coroutine)
        {
            while (IsPaused == true)
            {
                if (IsStopped)
                    yield break;

                yield return null;
            }

            if (coroutine.MoveNext())
                yield return coroutine.Current;
            else
                yield break;
        }
#endregion
    }
}
