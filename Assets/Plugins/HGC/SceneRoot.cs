﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HGC.Utils;
using HGC;
using System.Linq;

namespace HGC.Scenes
{
    public class SceneRoot : MonoBehaviour
    {
        public static readonly string Game_Scene = "Game";
        public static readonly string UI_Scene = "UI";
        public static readonly string Main_Scene = "Main";

        public GameObject Root { get; private set; }
        public GameObject UI { get; private set; }

        [SerializeField]
        private string _sceneName;
        
        // not underscore because of the custom inspector... naming conventions..
        [SerializeField, Scene]
        private List<string> dependencies = new List<string>();

        public string SceneName                         { get { return _sceneName; } }
        public IEnumerable<string> Dependencies         { get { return dependencies; } }
        public bool IsSceneSetup()                      { return ( _gameLogicManager != null); }

        private GameLogicManager _gameLogicManager;

        IEnumerator Start()
        {
            _gameLogicManager = GameLogicManager.Instance;

            Log.Info("Scene '" + SceneName + "' is loading dependencies: " + string.Join(",", Dependencies.ToArray()));

            yield return StartCoroutine(SceneManager.LoadScenes(Dependencies));     
        }

#if UNITY_EDITOR
        private static bool _hasFirstSceneLoaded = false;

        [UnityEditor.Callbacks.PostProcessScene]
        public static void OnPostProcessScene()
        {
            if (!_hasFirstSceneLoaded)
            {
                _hasFirstSceneLoaded = true;
            }
        }

        void Reset()
        {
            Log.WarnIf("Scene", string.IsNullOrEmpty(_sceneName), "Scene is not named!");
            dependencies = new List<string>();

            Log.Assert(gameObject.transform.parent == null, "Scene", "Scene Root Component is not attached to a root object!");
        }
#endif
    }
}
