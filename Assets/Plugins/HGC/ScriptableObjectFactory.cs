﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HGC
{
    public static class ScriptableObjectFactory
    {
#if UNITY_EDITOR
        [MenuItem("Assets/Create/Scriptable Object"), MenuItem("HGC/Tools/Scriptable Object/Create")]
        public static void CreateInWindow()
        {
            var assembly = GetAssembly();

            // Get all classes derived from ScriptableObject
            var allScriptableObjects = (from t in assembly.GetTypes()
                                        where t.IsSubclassOf(typeof(ScriptableObject))
                                        select t).ToArray();

            // Show the selection window.
            ScriptableObjectWindow.Init(allScriptableObjects);
        }

        [MenuItem("HGC/Tools/Scriptable Object/Create Something")]
        [UsedImplicitly]
        public static void CreateSomething()
        {
            
        }

        public static void CreateAsset<T>() where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();

            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (path == "")
            {
                path = "Assets";
            }
            else if (Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }

            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }

        /// <summary>
        /// Returns the assembly that contains the script code for this project (currently hard coded)
        /// </summary>
        private static Assembly GetAssembly()
        {
            return Assembly.Load(new AssemblyName("Assembly-CSharp"));
        }

#endif
    }
}