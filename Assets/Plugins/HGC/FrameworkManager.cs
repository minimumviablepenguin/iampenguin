﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HGC
{
    public class FrameworkManager : Singleton<FrameworkManager>
    {
        [SerializeField]
        private GameObject _managersParent;

        public GameObject ManagersParent { get { return _managersParent; } }

        public void Reset()
        {
            _managersParent = gameObject.FindChild("Managers");

            if (_managersParent != null)
            {
                // TODO - For Clean singleton shutdown
            }
        }
    }
}
