﻿using UnityEngine;
using System;
using System.Collections;

namespace HGC.CustomEventSystem{
	[System.Serializable]
	public class EventParameter {
		[SerializeField]
		private string type;
		public Type SerializedType{
			get{
				string[] split=type.Split('.');
				return Type.GetType(type+(split[0]=="UnityEngine"?",UnityEngine":""));	
			}
			set{
				type=value.ToString();
			}
		}
		public string name;

		public EventReferenceEntry entry;
		public bool useReference;

		public UnityEngine.Object objParam;
		public int intParam;
		public float floatParam;
		public AnimationCurve curveParam;
		public string stringParam;
		public Vector2 vector2Param;
		public Vector3 vector3Param;
		public Transform transformParam;
		public bool boolParam;
		public Vector4 vector4Param;
		public Color colorParam;
		public Rect rectParam;
		public Space spaceParam;
		public Quaternion quaternionParam;
		public SendMessageOptions sendMessageOptionsParam;
		
		public EventParameter(string name,string type) {
			this.type=type;
			this.name=name;
		}
		
		public object Get(){
			if (useReference) {
				return entry.GetValue();
			} else {
				switch (type) {
				case "System.Int16":
				case "System.Int32":
				case "System.Int64":
					return intParam;
				case "System.Single":
				case "System.Double":
					return floatParam;
				case "System.String":
					return stringParam;
				case "System.Boolean":
					return boolParam;
				case "UnityEngine.Vector2":
					return vector2Param;
				case "UnityEngine.Vector3":
					return vector3Param;
				case "UnityEngine.Vector4":
					return vector4Param;
				case "UnityEngine.Color":
					return colorParam;
				case "UnityEngine.Rect":
					return rectParam;
				case "UnityEngine.AnimationCurve":
					return curveParam;
				case "UnityEngine.Space":
					return spaceParam;
				case "UnityEngine.Quaternion":
					return quaternionParam;
				case "UnityEngine.SendMessageOptions":
					return sendMessageOptionsParam;
				default:
					return objParam;
					
				}
			}
		}
		
	}
}
