using UnityEngine;

public static class LayerMaskHelper
{   
	public static bool Contains(this LayerMask mask, int layer)
	{
	 return ((mask.value & (1 << layer)) > 0);
	}

	public static bool Contains(this LayerMask mask, GameObject obj)
	{
	 return ((mask.value & (1 << obj.layer)) > 0);
	}
	
    public static int Show(int mask, params string[] layers)
    {
        for (int i = 0; i < layers.Length; i++ )
            mask |= 1 << LayerMask.NameToLayer(layers[i]);

        return mask;
    }

    public static int Hide(int mask, params string[] layers)
    {
        for (int i = 0; i < layers.Length; i++)
            mask &= ~(1 << LayerMask.NameToLayer(layers[i]));

        return mask;
    }

    public static int Toggle(int mask, params string[] layers)
    {
        for (int i = 0; i < layers.Length; i++)
            mask ^= 1 << LayerMask.NameToLayer(layers[i]);

        return mask;
    }
	
    public static int EverythingBut(params string[] layers)
    {
        int[] layerIDs = new int[layers.Length];

        for (int i = 0; i < layers.Length; i++)
            layerIDs[i] = LayerMask.NameToLayer(layers[i]);

        return EverythingBut(layerIDs);
    }


    private static int EverythingBut(params int[] layers)
    {
		return ~MakeMask(layers);
	}
	
	private static int MakeMask(params int[] layers)
    {
		int mask = 0;

		for(int i=0; i<layers.Length; i++)
        {
            mask |= 1 << layers[i];
		}

		return mask;	
	}
	
	/* shadow puppeteer examples...
	
	// cache for later - max 4 ignored layers for deferred lighting
    private static int _realtimeLightLayerMask;
    private static int _flickeringLightLayerMask;
    private static int _3DCollisionExcludeLayers;

    public static readonly int COLLIDEABLE_SHADOW = LayerMask.NameToLayer("CollideableShadow");
    public static readonly int WALKABLE_FOR_SHADOW = LayerMask.NameToLayer("WalkableForShadow");
	
	   public static int GetRealTimeLightLayerMask()
    {
        if(_realtimeLightLayerMask == 0)
        {
            // add collideableshadow and walkableshadow - maybe, and bake the scene that would be great :)
            _realtimeLightLayerMask = EverythingBut("Water");
        }
       
        return _realtimeLightLayerMask;
    }
	
    public static int Get3DCollisionExcludeLayers()
    {
        if(_3DCollisionExcludeLayers == 0)
        {
             _3DCollisionExcludeLayers = EverythingBut("Default", "CollideableShadow", "RealTimeShadowcaster", "WalkableForShadow", "Static3DGeometry", "Dynamic3DGeometry", "Ignore Raycast", "PushableCollider");
        }

        return _3DCollisionExcludeLayers;
    }

    public static int GetFlickeringLightLayerMask()
    {
        if (_flickeringLightLayerMask == 0)
        {
            _flickeringLightLayerMask = EverythingBut("Default", "Water", "CollideableShadow", "WalkableForShadow");
        }

        return _flickeringLightLayerMask;
    }
*/
}
