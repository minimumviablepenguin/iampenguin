﻿using UnityEngine;
using System.Collections;

namespace HGC
{
    public interface ICommand
    {
        // Update is called once per frame
        void Execute();
    }
}
