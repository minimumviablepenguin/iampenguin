﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HGC.Utils
{
    public enum ScenePathType
    {
        Full,
        AssetPath,
        BeneathScenePath,
        NameOnly
    }

    // Note - The Seemless Scene Package has its own attributes
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
    public class MessageAttribute : PropertyAttribute
    {

    }

    // Note - The Seemless Scene Package has its own attributes
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
    public class SceneAttribute : PropertyAttribute
    {
        public const string Extension = ".unity";
        public const string SceneFolder = "Assets/Scenes";
        
        public static string SceneFolderFilepath {  get { return Application.dataPath + "/Scenes"; } }
       

        public bool IncludeTestScenes;
        public string Filter;

#if UNITY_EDITOR
        public static string[] GetAll(ScenePathType pathType = ScenePathType.Full, string filter = null, bool includeTestScenes = false)
        {
            try
            {
                var sceneFilepaths = FileSystemUtilities.FindFilesInDirectories(SceneFolderFilepath, Extension, includeTestScenes);

                switch(pathType)
                {
                    case ScenePathType.Full: break;
                    case ScenePathType.AssetPath:
                    for (int i = 0; i < sceneFilepaths.Count; i++)
                        sceneFilepaths[i] = sceneFilepaths[i].Remove(0, Application.dataPath.Length - 6).Replace(Extension, "");
                    break;
                case ScenePathType.BeneathScenePath:
                    for (int i = 0; i < sceneFilepaths.Count; i++)
                        sceneFilepaths[i] = sceneFilepaths[i].Remove(0, SceneFolderFilepath.Length + 1).Replace(Extension, "");
                    break;
                case ScenePathType.NameOnly:
                    for (int i = 0; i < sceneFilepaths.Count; i++)
                        sceneFilepaths[i] = sceneFilepaths[i].Substring(sceneFilepaths[i].IndexOf("/")).Replace(Extension, "");
                    break;
                }      

                if (string.IsNullOrEmpty(filter))
                    return sceneFilepaths.ToArray();

                var filteredPaths = new List<string>();
                for (int i = 0; i< sceneFilepaths.Count; i++)
                {
                    if (sceneFilepaths[i].Contains(filter))
                        filteredPaths.Add(sceneFilepaths[i]);
                }

                return filteredPaths.ToArray();
            }
            catch(System.Exception e)
            {
                Log.Error("Build", "Failed to add scenes to build: " + e.Message + "\n\n" + e.StackTrace);    
            }

            return new string[0];
        }

#endif
    }

    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
    public class TagGroupAttribute : PropertyAttribute
    {
        public bool AllowUntagged;

        public static bool IsTag(string tag)
        {
#if UNITY_EDITOR
            return UnityEditorInternal.InternalEditorUtility.tags.Contains(tag);

#else
            throw new System.NotImplementedException("Could not get tag outside of editor code");
#endif
        }

        public static int GetIndex(string tag)
        {
#if UNITY_EDITOR
            int length = UnityEditorInternal.InternalEditorUtility.tags.Length;
            for (int i = 0; i < length; i++)
            {
                if (UnityEditorInternal.InternalEditorUtility.tags[i] == tag)
                    return i;
            }
            return -1;

#else
            throw new System.NotImplementedException("Could not get position of tag outside of editor code");
#endif
        }
    }       

    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class RemoteCallAttribute : System.Attribute
    {
        public static object CallMethod(GameObject target, string methodName, object[] args)
        {
            if (target == null) return null;

            var arglen = (args != null) ? args.Length : 0;
            var attribType = typeof(RemoteCallAttribute);
            const BindingFlags methodBinding = (BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var comp in target.GetComponents<Component>())
            {
                var methods = comp.GetType().GetMethods(methodBinding);

                foreach (var meth in methods)
                {
                    if (meth.Name != methodName) continue;

                    var attrib = System.Attribute.GetCustomAttribute(meth, attribType);
                    if (attrib == null) continue;

                    var paramInfos = meth.GetParameters();
                    if (paramInfos.Length != arglen) continue;

                    bool bFail = false;
                    for (int i = 0; i < arglen; i++)
                    {
                        if (!TypeUtil.IsType(args[i].GetType(), paramInfos[i].ParameterType))
                        {
                            bFail = true;
                            break;
                        }
                    }

                    if (bFail) continue;

                    return meth.Invoke(comp, args);
                }
            }

            return null;
        }
    }
}
