﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using HGC;
using HGC.Events;

namespace HGC.Utils
{
    [CustomPropertyDrawer(typeof(MessageAttribute))]
    public class MessagePropertyDrawer : PropertyDrawer
    {
        static string[] _typeNames;
        static Type[] _eventTypes;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.String)
            {
                EditorGUI.BeginProperty(position, label, property);

                //var attrib = this.attribute as MessageAttribute;

                if (_eventTypes == null)
                {
                    var allTypes = TypeUtil.GetTypesAssignableFrom(typeof(IMessageDispatcher)).ToList();

                    allTypes.Remove(typeof(BaseMessageDispatcher));
                    allTypes.Remove(typeof(IMessageDispatcher));

                    _eventTypes = allTypes.ToArray();
                }

                if (_typeNames == null)
                {
                    _typeNames = new string[_eventTypes.Length];

                    for (int i = 0; i < _eventTypes.Length; i++)
                    {
                        _typeNames[i] = _eventTypes[i].Name;
                    }
                }

                var propStr = property.stringValue;
                
                int index = -1;
                for (int i = 0; i < _typeNames.Length; i++)
                {
                    if (_typeNames[i] == propStr)
                    {
                        index = i;
                        break;
                    }
                }

                if(index >= 0 || string.IsNullOrEmpty(propStr))
                {
                    index = EditorGUI.Popup(position, label.text, index, _typeNames);
                    if (index >= 0)
                    {
                        property.stringValue = _typeNames[index];
                    }
                    else
                    {
                        property.stringValue = null;
                    }
                }
                else
                {
                    // do nothing, keep the out of sync value, so the person can repair the damage
                    Log.Error("Serialised Type derived from '" + typeof(IEventSystemHandler) + "' no longer exists: " + propStr);
                }

                EditorGUI.EndProperty();
            }
            else
            {
                EditorGUI.PropertyField(position, property, label);
            }
        }
    }

    [CustomPropertyDrawer(typeof(SceneAttribute))]
    public class ScenePropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.String)
            {
                EditorGUI.BeginProperty(position, label, property);

                var attrib = this.attribute as SceneAttribute;

                var scenes = SceneAttribute.GetAll(ScenePathType.BeneathScenePath, attrib.Filter, attrib.IncludeTestScenes);

                if(scenes.Length == 0)
                {
                    Log.Warn("Utils", "No Scenes found in the folder: " + SceneAttribute.SceneFolderFilepath);
                    return;
                }

                var propStr = property.stringValue;

                int index = -1;
                for (int i = 0; i < scenes.Length; i++)
                {
                    if (scenes[i] == propStr)
                    {
                        index = i;
                        break;
                    }
                }
                index = EditorGUI.Popup(position, label.text, index, scenes);
                if (index >= 0)
                {
                    property.stringValue = scenes[index];
                }
                else
                {
                    if (!string.IsNullOrEmpty(propStr))
                    {
                        // do nothing, keep the out of sync value, so the person can repair the damage
                        Log.ErrorOnce("SerializedProperty", "SerializedProperty."+ label.text, "Serialised value for tag no longer exists! Tag was renamed or deleted: '" + propStr + "'");
                    }
                }

                EditorGUI.EndProperty();
            }
            else
            {
                EditorGUI.PropertyField(position, property, label);
            }
        }
    }

    [CustomPropertyDrawer(typeof(TagGroupAttribute))]
    public class TagPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.String)
            {
                Log.Info("hah", "not string");
                EditorGUI.PropertyField(position, property, label);
            }
            else
            {
                EditorGUI.BeginProperty(position, label, property);

                var attrib = this.attribute as TagGroupAttribute;

                if (attrib.AllowUntagged)
                {
                    property.stringValue = EditorGUI.TagField(position, label, property.stringValue);
                }
                else
                {
                    var propStr = property.stringValue;
                    var tags = UnityEditorInternal.InternalEditorUtility.tags;

                    int index = -1;
                    for (int i = 0; i < tags.Length; i++)
                    {
                        if (tags[i] == propStr)
                        {
                            index = i;
                            break;
                        }
                    }
                    index = EditorGUI.Popup(position, label.text, index, tags);
                    if (index >= 0)
                    {
                        property.stringValue = tags[index];
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(propStr))
                        {
                            Log.Warn("SerializedProperty", "No Serialised value for tag : '" + label + "' defaulting to untagged...");
                            property.stringValue = tags[0];
                        }
                        else
                        {
                            // do nothing, keep the out of sync value, so the person can repair the damage
                            Log.Error("SerializedProperty", "Serialised value for tag no longer exists! Tag was renamed or deleted: '" + propStr + "'");
                        }
                    }
                }

                EditorGUI.EndProperty();
            }
        }         
    }
}
