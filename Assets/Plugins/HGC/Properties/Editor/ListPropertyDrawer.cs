﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace HGC.Utils
{
    public class ListPropertyDrawer : AbstractCustomPropertyDrawer
    {
        ReorderableList list;

        public ListPropertyDrawer(string propertyName) : base(propertyName)
        {}

        public override void OnEnable(SerializedObject serializedObject, SerializedProperty serializedProperty)
        {
            list = new ReorderableList(serializedObject,
                            serializedProperty,
                            true,
                            true,
                            true,
                            true);

            list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = list.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;

                // height is a fixed size here, abstract class has height accessors
                EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, rect.height), element, new GUIContent(Name + " " + (index + 1)), true);
            };


            list.drawHeaderCallback = (Rect rect) => {
                EditorGUI.LabelField(rect, Name);
            };

            list.onCanRemoveCallback = (ReorderableList l) => {
                return l.count > 0;
            };
        }

        public override void OnInspectorGUI(SerializedObject serializedObject, SerializedProperty serializedProperty)
        {
            serializedObject.Update();
            list.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
