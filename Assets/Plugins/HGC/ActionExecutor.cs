﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HGC.CustomEventSystem{
	public class EventExecutor : MonoBehaviour {
		public ExecuteInside runInside;
		public int mouseButton;
		public KeyCode keyCode;
		public GameObject target;
		public List<CustomEvent> actions= new List<CustomEvent>();
		
		public void Awake(){
			if (runInside == ExecuteInside.OnAwake) {
				Execute();
			}
		}
		
		public void Start(){
			if (runInside == ExecuteInside.OnStart) {
				Execute();
			}
		}
		
		public void Update(){
			switch (runInside) {
			case ExecuteInside.OnUpdate:
				Execute();
				break;
			case ExecuteInside.OnGetMouseButton:
				if(Input.GetMouseButton(mouseButton))
					Execute();
				break;
			case ExecuteInside.OnGetMouseButtonDown:
				if(Input.GetMouseButtonDown(mouseButton))
					Execute();
				break;
			case ExecuteInside.OnGetMouseButtonUp:
				if(Input.GetMouseButtonUp(mouseButton))
					Execute();
				break;
			case ExecuteInside.OnGetKey:
				if(Input.GetKey(keyCode))
					Execute();
				break;
			case ExecuteInside.OnGetKeyDown:
				if(Input.GetKeyDown(keyCode))
					Execute();
				break;
			case ExecuteInside.OnGetKeyUp:
				if(Input.GetKeyUp(keyCode))
					Execute();
				break;
			}
		}
		
		public void FixedUpdate(){
			if (runInside == ExecuteInside.OnFixedUpdate) {
				Execute();
			}
		}
		
		public void LateUpdate(){
			if (runInside == ExecuteInside.OnLateUpdate) {
				Execute();
			}
		}

		private void OnMouseDown(){
			if (runInside == ExecuteInside.OnMouseDown) {
				Execute();
			}
		}
		
		private void OnMouseDrag(){
			if (runInside == ExecuteInside.OnMouseDrag) {
				Execute();
			}
		}
		
		private void OnMouseEnter(){
			if (runInside == ExecuteInside.OnMouseEnter) {
				Execute();
			}
		}
		
		private void OnMouseExit(){
			if (runInside == ExecuteInside.OnMouseExit) {
				Execute();
			}
		}
		
		private void OnMouseOver(){
			if (runInside == ExecuteInside.OnMouseOver) {
				Execute();
			}
		}
		
		private void OnMouseUp(){
			if (runInside == ExecuteInside.OnMouseUp) {
				Execute();
			}
		}
		
		private void OnTriggerEnter(Collider other){
			if (runInside == ExecuteInside.OnTriggerEnter) {
				Execute();
			}
		}
		
		private void OnTriggerEnter2D(Collider2D other){
			if (runInside == ExecuteInside.OnTriggerEnter2D) {
				Execute();
			}
		}
		
		private void OnTriggerExit(Collider other){
			if (runInside == ExecuteInside.OnTriggerExit) {
				Execute();
			}
		}
		
		private void OnTriggerExit2D(Collider2D other){
			if (runInside == ExecuteInside.OnTriggerExit2D) {
				Execute();
			}
		}
		
		private void OnTriggerStay(Collider other){
			if (runInside == ExecuteInside.OnTriggerStay) {
				Execute();
			}
		}
		
		private void OnTriggerStay2D(Collider2D other){
			if (runInside == ExecuteInside.OnTriggerStay2D) {
				Execute();
			}
		}
		
		public void OnDrawGizmos(){
			if (runInside == ExecuteInside.OnDrawGizmos) {
				Execute();
			}
		}
		
		public void Execute(){
			foreach (CustomEvent action in actions) {
				action.Execute ((target==null?gameObject:target));		
			}
		}
		
	}
	
	public enum ExecuteInside{
		OnAwake,
		OnStart,
		OnUpdate,
		OnFixedUpdate,
		OnLateUpdate,
		/*OnAnimatorIK,
	OnAnimatorMove,
	OnApplicationFocus,
	OnApplicationPause,
	OnApplicationQuit,
	OnAudioFilterRead,
	OnBecameInvisible,
	OnBecameVisible,
	OnCollisionEnter,
	OnCollisionEnter2D,
	OnCollisionExit,
	OnCollisionExit2D,
	OnCollisionStay,
	OnCollisionStay2D,
	OnConnectedToServer,
	OnControllerColliderHit,
	OnDestroy,
	OnDisable,
	OnDisconnectedFromServer,*/
		OnDrawGizmos,
		/*OnDrawGizmosSelected,
	OnEnable,
	OnFailedToConnect,
	OnFailedToConnectToMasterServer,
	OnGUI,
	OnJointBreak,
	OnLevelWasLoaded,
	OnMasterServerEvent,*/
		OnMouseDown,
		OnMouseDrag,
		OnMouseEnter,
		OnMouseExit,
		OnMouseOver,
		OnMouseUp,
		/*OnMouseUpAsButton,
	OnNetworkInstantiate,
	OnParticleCollision,
	OnPlayerConnected,
	OnPlayerDisconnected,
	OnPostRender,
	OnPreCull,
	OnPreRender,
	OnRenderImage,
	OnRenderObject,
	OnSerializeNetworkView,
	OnServerInitialized,*/
		OnTriggerEnter,
		OnTriggerEnter2D,
		OnTriggerExit,
		OnTriggerExit2D,
		OnTriggerStay,
		OnTriggerStay2D,
		/*OnValidate,
	OnWillRenderObject,*/
		OnGetMouseButtonDown,
		OnGetMouseButtonUp,
		OnGetMouseButton,
		OnGetKey,
		OnGetKeyDown,
		OnGetKeyUp,
		
	}
}