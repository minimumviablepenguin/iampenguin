﻿using UnityEngine;

namespace HGC
{
    /// <summary>
    /// This is a utility for managing different path types that Unity uses in its APIs. Note this is string op heavy.
    /// During the build we probably only want to load via reference/guid or resource
    /// </summary>
	public class EditorPath
	{
		public string PostProcessPath { get; private set; }
		public string FilePath { get; private set; }
		public string Filename { get; private set; }
		public string Extension { get; private set; }

		public string FoldersString { get; private set; }
		public string[] Folders { get; private set; }

		private static string projectFolder = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf("/Assets")) + "/";
		public static string ProjectFolder { get { return projectFolder; } }
		                        
		private string assetPath;
		public string AssetPath 
		{
			get
			{
				if(string.IsNullOrEmpty(assetPath))
					assetPath = FileToAssetPath(FilePath);

				return assetPath;
			}
		}

		// this should also be an attribute
		public EditorPath(string postProcessPath)
		{
#if UNITY_EDITOR
            Log.Error("Utils", "You are using an Editor Path outside of the unity editor.");
#endif

            string[] splitStr = assetPath.Split('/', '.');

			PostProcessPath = postProcessPath;
			FoldersString = GetFolderPath(splitStr);
			Filename = splitStr[splitStr.Length - 2];
			Extension = splitStr[splitStr.Length - 1];
			
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(ProjectFolder);
			sb.Append ("/");
			sb.Append(Folders);
			sb.Append(Filename);
			sb.Append(".");
			sb.Append(Extension);
			FilePath = sb.ToString();
		}

		private string GetFolderPath(string[] pathParts)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder("");
			
			int folders = pathParts.Length - 2;
			Folders = new string[folders];
			for (int i=0; i<folders; i++)
			{
				Folders[i] = pathParts[i];

				sb.Append(pathParts[i]);
				sb.Append("/");
			}
			
			return sb.ToString();
		}

        public static string GetExtension(string path)
        {
            int index = path.LastIndexOf(".");
            if (index != -1)
                return path.Substring(index + 1);
            else
                return null;
        }

		public static string AssetToFilepath(string assetPath)
		{
			if (!assetPath.StartsWith ("Assets"))
			{
				Log.Error("Utils", "Asset path did not start with Assets, this is not an asset path.");
			}

			return ProjectFolder + "/" + assetPath;
		}

        public static string FileToPostProcessPath(string filePath)
        {
            if(filePath.StartsWith("Assets"))
            {
                Log.Warn("Utils", "Filepath was not a filepath since it began with Assets.");
                return filePath;
            }

            int index = filePath.IndexOf("Assets");
            if (index == -1)
            {
                Log.Warn("Utils", "Filepath did not contain the string 'Assets'. Could not form a unity PostProcessPath");
                return null;
            }

            return filePath.Substring(index);
        }

        public static string FileToAssetPath(string filePath)
		{
			if(filePath.StartsWith("Assets"))
			{
				Log.Warn("Utils", "Filepath was actually an AssetPath. Conversion was unneccessary.");
				return filePath;
			}

			return "Assets" + filePath.Replace(Application.dataPath, "");
		}
	}
}
