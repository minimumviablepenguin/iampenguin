﻿namespace HGC
{
    public interface IRelease
    {
        void Release();
    }
}
