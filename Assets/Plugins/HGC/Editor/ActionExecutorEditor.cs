﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace HGC.CustomEventSystem{
	[CustomEditor(typeof(EventExecutor),true)]
	public class EventExecutorEditor : Editor {
		private EventExecutor executor;
		private CustomEventSystem.ReorderableList list;
		private List<Type> types;
		
		private void OnEnable(){
			executor = (EventExecutor)target;
			list = new CustomEventSystem.ReorderableList ( executor.actions,"Event", true, true);
			list.onAddCallback = OnAddAction;
			list.drawElementCallback = DrawActionElement;
			
			List<Component> components = (executor.target != null?executor.target:executor.gameObject).GetComponents<Component> ().ToList ();
			types = components.Select (x => x.GetType ()).ToList();
			types.Add (typeof(UnityEngine.Object));
			types.Add (typeof(Application));
			types.Add (typeof(Debug));
			types.Add (typeof(PlayerPrefs));
			types.Add (typeof(Time));
			types.Add (typeof(Gizmos));
			types.Add (typeof(Vector3));
			types.Add (typeof(Mathf));
		}
		
		
		public override void OnInspectorGUI ()
		{
			GUI.changed = false;
			GUILayout.Space (5);
			executor.runInside = (ExecuteInside)EditorGUILayout.EnumPopup ("Run Inside", executor.runInside);
			switch (executor.runInside) {
			case ExecuteInside.OnGetMouseButton:
			case ExecuteInside.OnGetMouseButtonDown:
			case ExecuteInside.OnGetMouseButtonUp:
				executor.mouseButton=EditorGUILayout.IntField("Button",executor.mouseButton);
				break;
			case ExecuteInside.OnGetKey:
			case ExecuteInside.OnGetKeyDown:
			case ExecuteInside.OnGetKeyUp:
				executor.keyCode=(KeyCode)EditorGUILayout.EnumPopup("Key",executor.keyCode);
				break;
			}
			executor.target = (GameObject)EditorGUILayout.ObjectField ("Target", executor.target, typeof(GameObject), true);
			GUILayout.Space (5);
			list.DoList ();
			if (GUI.changed) {
				OnEnable();
				EditorUtility.SetDirty(executor);		
			}
		}
		
		private void OnAddAction(){
			GenericMenu genericMenu = new GenericMenu ();
			foreach (Type mType in types) {
				List<MethodInfo> mMethods=mType.GetMethods(BindingFlags.Public|BindingFlags.Instance).ToList();
				mMethods.AddRange(mType.GetMethods(BindingFlags.Public|BindingFlags.Static));
				
				for (int b = 0; b < mMethods.Count; ++b)
				{
					MethodInfo mi = mMethods[b];
					
					string name = mi.Name;
					if (name == "Invoke") continue;
					if (name == "InvokeRepeating") continue;
					if (name == "CancelInvoke") continue;
					if (name == "StopCoroutine") continue;
					if (name == "StopAllCoroutines") continue;
					if (name == "BroadcastMessage") continue;
					//if (name.StartsWith("SendMessage")) continue;
					//if (name.StartsWith("get_")) continue;
					if(!IsValid(mType,name))continue;
					
					genericMenu.AddItem(new GUIContent(mType.ToString().Split('.').Last()+"/"+name),false,AddAction,new object[]{mType,name});
					
				}
			}
			genericMenu.ShowAsContext ();
			Event.current.Use ();
		}
		
		
		
		private void AddAction(object data){
			object[] mData = (object[])data;
			
			CustomEvent action = new CustomEvent ((Type)mData[0],(string)mData[1]);
			action.parameters = new List<EventParameter> ();
			
			ParameterInfo[] parameters =GetOverloadingParameters (action.SerializedType,action.method)[action.selectedOverloadMethodIndex];
			foreach(ParameterInfo info in parameters){
				action.parameters.Add(new EventParameter(UppercaseFirst(info.Name),info.ParameterType.ToString()));
			}
			executor.actions.Add (action);
		}
		
		
		private void DrawActionElement(int index){
			CustomEvent action = executor.actions [index];
			
			GUILayout.BeginVertical ();
			
			EditorGUIUtility.labelWidth = 80;
			
			GUI.changed = false;
			action.selectedOverloadMethodIndex=EditorGUILayout.Popup("Method",action.selectedOverloadMethodIndex,GetOverloadingMethodNames(action.SerializedType,action.method));
			
			if (GUI.changed) {
				action.parameters.Clear();
				ParameterInfo[] parameters = GetOverloadingParameters (action.SerializedType,action.method)[action.selectedOverloadMethodIndex];
				foreach(ParameterInfo info in parameters){
					action.parameters.Add(new EventParameter(UppercaseFirst(info.Name),info.ParameterType.ToString()));
				}
			}

			foreach (EventParameter p in action.parameters) {
				GUILayout.BeginHorizontal();
				if(!p.useReference){
					switch(p.SerializedType.ToString())
					{
					case "System.Int16":
					case "System.Int32":
					case "System.Int64":
						p.intParam=EditorGUILayout.IntField(p.name,p.intParam);
						break;
					case "System.Single":
					case "System.Double":
						p.floatParam=EditorGUILayout.FloatField(p.name,p.floatParam);
						break;
					case "System.String":
						p.stringParam=EditorGUILayout.TextField(p.name,p.stringParam);
						break;
					case "System.Boolean":
						p.boolParam=EditorGUILayout.Toggle(p.name,p.boolParam);
						break;
					case "UnityEngine.Vector2":
						p.vector2Param=EditorGUILayout.Vector2Field(p.name,p.vector2Param);
						break;
					case "UnityEngine.Vector3":
						p.vector3Param=EditorGUILayout.Vector3Field(p.name,p.vector3Param);
						break;
					case "UnityEngine.Vector4":
						p.vector4Param=EditorGUILayout.Vector4Field(p.name,p.vector4Param);
						break;
					case "UnityEngine.Color":
						p.colorParam=EditorGUILayout.ColorField(p.name,p.colorParam);
						break;
					case "UnityEngine.Rect":
						p.rectParam=EditorGUILayout.RectField(p.name,p.rectParam);
						break;
						
					case "UnityEngine.PrimitiveType": 
						break;
						
					case "UnityEngine.AnimationCurve":
						p.curveParam=EditorGUILayout.CurveField(p.name,p.curveParam);
						break;
					case "UnityEngine.Space":
						p.spaceParam=(Space)EditorGUILayout.EnumPopup(p.name,p.spaceParam);
						break;
					case "UnityEngine.Quaternion":
						p.vector3Param=EditorGUILayout.Vector3Field(p.name,p.vector3Param);
						p.quaternionParam=Quaternion.Euler(p.vector3Param);
						break;
					case "UnityEngine.SendMessageOptions":
						p.sendMessageOptionsParam=(SendMessageOptions)EditorGUILayout.EnumPopup(p.name,p.sendMessageOptionsParam);
						break;
					default:
						if(p.SerializedType==typeof(UnityEngine.Object) || p.SerializedType.IsSubclassOf( typeof(UnityEngine.Object) ) )
						{
							p.objParam=EditorGUILayout.ObjectField(p.name,p.objParam,p.SerializedType,true);
						}else{
							GUILayout.Label("Unsupported Type: "+p.SerializedType.ToString());
						}
						break;
					}
				}else{
					GUILayout.BeginVertical();
					p.entry.target=(GameObject)EditorGUILayout.ObjectField("Target",p.entry.target,typeof(GameObject),true);
					if(p.entry.target != null){
						Component[] refComponents=p.entry.target.GetComponents<Component>();
						string[] refComponentNames=refComponents.Select(x=>x.GetType().ToString().Split('.').Last()).ToArray();
						StringPopup("Component",ref p.entry.component,refComponentNames);
						
						List<EventReferenceEntry> properties = GetProperties (p.entry.target.GetComponent(p.entry.component).GetType(), p.SerializedType);
						string[] fieldNames = properties.Select(x=>x.property).ToArray();
						StringPopup("Field",ref p.entry.property,fieldNames);
						
					}
					GUILayout.EndVertical();
				}
	
				p.useReference=GUILayout.Toggle(p.useReference,GUIContent.none,GUILayout.Width(20));
				GUILayout.EndHorizontal();
			}
			
			GUILayout.BeginVertical ("box");
			Type returnType = GetReturnType (action.SerializedType, action.method);
			GUILayout.Label ("Return: "+returnType.ToString());
			if (returnType != typeof(void)) {
				action.entry.target=(GameObject)EditorGUILayout.ObjectField("Target",action.entry.target,typeof(GameObject),true);
				if(action.entry.target != null){
					Component[] refComponents=action.entry.target.GetComponents<Component>();
					string[] refComponentNames=refComponents.Select(x=>x.GetType().ToString().Split('.').Last()).ToArray();
					StringPopup("Component",ref action.entry.component,refComponentNames);
					
					List<EventReferenceEntry> properties = GetProperties (action.entry.target.GetComponent(action.entry.component).GetType(), returnType);
					string[] fieldNames = properties.Select(x=>x.property).ToArray();
					StringPopup("Field",ref action.entry.property,fieldNames);
					
				}
			}
			GUILayout.EndVertical ();
			GUILayout.EndVertical ();
			Repaint ();
		}
		
		public static void StringPopup(string label,ref string value, string[] list){
			int index=0;
			if(list.Length>0){
				for(int cnt=0; cnt<list.Length;cnt++){
					if(value == list[cnt]){
						index=cnt;
					}
				}
				index=EditorGUILayout.Popup(label,index,list);
				value=list[index];
			}
		}
		
		static string UppercaseFirst(string s)
		{
			if (string.IsNullOrEmpty(s)){
				return string.Empty;
			}
			return char.ToUpper(s[0]) + s.Substring(1);
		}
		
		public Type GetReturnType(Type classType, string method){
			List<MethodInfo> mMethods=classType.GetMethods(BindingFlags.Public|BindingFlags.Instance).ToList();
			mMethods.AddRange(classType.GetMethods(BindingFlags.Public|BindingFlags.Static));
			MethodInfo mi=mMethods.Find (x => x.Name == method);
			if (mi != null) {
				return mi.ReturnType;		
			}
			return null;
		}
		
		private string[] GetOverloadingMethodNames(Type type, string method){
			List<string> overloadingConstructorNames = new List<string> ();
			List<ParameterInfo[]> overloadingParameters = GetOverloadingParameters (type, method);
			for(int p=0; p< overloadingParameters.Count;p++){
				ParameterInfo[] info=overloadingParameters[p];
				string mConstructur="(";
				for(int i=0; i< info.Length;i++){
					mConstructur+=info[i].ParameterType.ToString().Split('.').Last() +" p"+i;
					if(i+1 < info.Length){
						mConstructur+=", ";
					}
				}
				mConstructur+=")";
				overloadingConstructorNames.Add(method+mConstructur);
			}
			return overloadingConstructorNames.ToArray ();
		}
		
		public List<EventReferenceEntry> GetProperties(Type type, Type returnType){
			List<EventReferenceEntry> properties = new List<EventReferenceEntry> ();
			
			BindingFlags flags = BindingFlags.Instance | BindingFlags.Public;
			FieldInfo[] fields = type.GetFields (flags);
			PropertyInfo[] props = type.GetProperties (flags);
			for (int b = 0; b < fields.Length; ++b)
			{
				FieldInfo field = fields[b];
				if(field.FieldType == returnType){
					EventReferenceEntry ent = new EventReferenceEntry();
					ent.component = type.ToString();
					ent.property = field.Name;
					properties.Add(ent);
				}
			}
			
			for (int b = 0; b < props.Length; ++b)
			{
				PropertyInfo prop = props[b];
				if(prop.PropertyType == returnType){
					EventReferenceEntry ent = new EventReferenceEntry();
					ent.component = type.ToString();
					ent.property = prop.Name;
					properties.Add(ent);
				}
			}
			
			return properties;
		}
		
		private List<ParameterInfo[]> GetOverloadingParameters(Type type, string method){
			List<ParameterInfo[]> overloadingParameters = new List<ParameterInfo[]> ();
			List<MethodInfo> mMethods = GetValidMethodInfo (type, method);
			
			for (int b = 0; b < mMethods.Count; ++b) {
				MethodInfo mi = mMethods [b];
				ParameterInfo[] info = mi.GetParameters ();
				overloadingParameters.Add (info);
			}
			
			return overloadingParameters;
		}
		
		
		public bool IsValid(Type type, string method){
			List<MethodInfo> mMethods = GetValidMethodInfo (type, method);
			
			for (int b = 0; b < mMethods.Count; ++b)
			{
				MethodInfo mi = mMethods[b];
				
				ParameterInfo[] info = mi.GetParameters();
				foreach(ParameterInfo p in info){
					
					switch(p.ParameterType.ToString())
					{
					case "System.Int16":
					case "System.Int32":
					case "System.Int64":
					case "System.Single":
					case "System.Double":
					case "System.String":
					case "System.Boolean":
					case "UnityEngine.Vector2":
					case "UnityEngine.Vector3":
					case "UnityEngine.Vector4":
					case "UnityEngine.Color":
					case "UnityEngine.Rect":
					case "UnityEngine.AnimationCurve":
					case "UnityEngine.Space":
					case "UnityEngine.Quaternion":
					case "UnityEngine.SendMessageOptions":
						return true;
					default:
						if(p.ParameterType==typeof(UnityEngine.Object) || p.ParameterType.IsSubclassOf( typeof(UnityEngine.Object) ) )
						{
							return true;
						}
						return false;
					}
				}
			}
			return true;
		}
		
		public List<MethodInfo> GetValidMethodInfo(Type type,string method){
			List<MethodInfo> validMethods = new List<MethodInfo> ();
			
			List<MethodInfo> mMethods=type.GetMethods(BindingFlags.Public|BindingFlags.Instance).ToList();
			mMethods.AddRange(type.GetMethods(BindingFlags.Public|BindingFlags.Static));
			
			for (int b = 0; b < mMethods.Count; ++b) {
				MethodInfo mi = mMethods [b];
				
				string name = mi.Name;
				if (name == "Invoke")continue;
				if (name == "InvokeRepeating")continue;
				if (name == "CancelInvoke")continue;
				if (name == "StopCoroutine")continue;
				if (name == "StopAllCoroutines")continue;
				if (name == "BroadcastMessage")continue;
				//if (name.StartsWith ("SendMessage"))continue;
				//if (name.StartsWith ("get_"))continue;
				if (name != method)continue;
				
				validMethods.Add(mi);
			}
			return validMethods;
		}
	}
}