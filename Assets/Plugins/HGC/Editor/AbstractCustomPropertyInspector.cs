﻿using System.Collections.Generic;
using UnityEditor;

namespace HGC.Utils
{
    public abstract class AbstractCustomPropertyInspector : Editor
    {
        private Dictionary<string, AbstractCustomPropertyDrawer> _customPropertyInspectors;

        protected Dictionary<string, AbstractCustomPropertyDrawer> CustomPropertyInspectors
        {
            get
            {
                if (_customPropertyInspectors != null)
                    return _customPropertyInspectors;

                _customPropertyInspectors = new Dictionary<string, AbstractCustomPropertyDrawer>();
                return _customPropertyInspectors;
            }
        }

        protected abstract void OnInitialise();

        private void OnEnable()
        {
            if (target == null)
                return;

            OnInitialise();

            foreach (var customPropertyName in _customPropertyInspectors.Keys)
            {
                EnableCustomPropertyInspector(serializedObject, serializedObject.FindProperty(customPropertyName));
            }        
        }

        public override void OnInspectorGUI()
        {
            var prop = serializedObject.GetIterator();
            if (prop.NextVisible(true))
            {
                DrawCustomPropertyInspector(serializedObject, prop);
            }

            while (prop.NextVisible(false))
            {
                DrawCustomPropertyInspector(serializedObject, prop);
            }        
        }

        private void EnableCustomPropertyInspector(SerializedObject serializedObject, SerializedProperty prop)
        {
            if (prop == null)
                return;

            if (CustomPropertyInspectors.ContainsKey(prop.name))
            {
                CustomPropertyInspectors[prop.name].OnEnable(serializedObject, prop);
            }
        }

        private void DrawCustomPropertyInspector(SerializedObject serializedObject, SerializedProperty prop)
        {
            if (prop == null)
                return;

            if (CustomPropertyInspectors.ContainsKey(prop.name))
            {
                CustomPropertyInspectors[prop.name].OnInspectorGUI(serializedObject, prop);
            }
            else
            {
                serializedObject.Update();
                EditorGUILayout.PropertyField(prop, true);
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}
