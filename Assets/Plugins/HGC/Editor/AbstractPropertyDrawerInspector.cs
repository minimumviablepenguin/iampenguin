﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using HGC;
using HGC.Events;

namespace HGC.Utils
{
    public abstract class AbstractCustomPropertyDrawer
    {
        protected AbstractCustomPropertyDrawer(string propertyName)
        {
            Name = propertyName;
        }

        protected string Name { get; private set; }

        public abstract void OnEnable(SerializedObject serializedObject, SerializedProperty serializedProperty);
        public abstract void OnInspectorGUI(SerializedObject serializedObject, SerializedProperty serializedProperty);

        public float GetElementHeight(SerializedProperty parentProperty, int index)
        {
            float height = 0f;
            SerializedProperty property = parentProperty.GetArrayElementAtIndex(index);
            SerializedObject obj = new SerializedObject(property.objectReferenceValue);
            property = obj.GetIterator();

            property.NextVisible(true);
            while (property.NextVisible(false))
            {
                height += EditorGUI.GetPropertyHeight(property) + 2f;
            }
            return height + 21f;
        }

        public float GetHeight(SerializedProperty parentProperty)
        {
            float height = 0f;
            for (int i = 0; i < parentProperty.arraySize; i++)
            {
                height += GetElementHeight(parentProperty, i);
            }
            return Mathf.Max(height, 21f) + 7f;
        }
    }
}
