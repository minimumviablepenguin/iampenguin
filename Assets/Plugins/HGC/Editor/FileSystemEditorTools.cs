﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using UnityEditor;

namespace HGC
{
    public static class FileSystemEditorTools
    {
        [MenuItem("Assets/Tools/Escape Special Chars")]
        [UsedImplicitly]
        public static void EscapeSpecialCharactersUnderAsset()
        {
            var directory = GetSelectedDirectoryPath();

            EscapeSpecialCharacters(".fbx", directory, "@", "__", "Escape");
        }

        [MenuItem("Assets/Tools/Unescape Special Chars")]
        [UsedImplicitly]
        public static void UnscapeSpecialCharactersUnderAsset()
        {
            var directory = GetSelectedDirectoryPath();
            EscapeSpecialCharacters(".fbx", directory, "__", "@", "Escape");
        }

        [MenuItem("HGC/Tools/Project/Escape Special Characters")]
        [UsedImplicitly]
        public static void UnescapeSpecialCharactersUnderAsset()
        {
            EscapeSpecialCharacters(".fbx", Application.dataPath, "@", "__", "Escape");
        }

        [MenuItem("HGC/Tools/Project/Unescape Special Characters")]
        [UsedImplicitly]
        public static void UnscapeSpecialCharacters()
        {
            EscapeSpecialCharacters(".fbx", Application.dataPath, "__", "@", "Un escape");
        }

        /// <summary>
        /// This will return the parent directory of the selection as a native filepath, or that of the root assets folder if nothing is selected
        /// </summary>
        /// <returns></returns>
        public static string GetSelectedDirectoryPath()
        {
            string path = string.Empty;

            foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
            {
                path = AssetDatabase.GetAssetPath(obj);
                if (!string.IsNullOrEmpty(path) && File.Exists(path))
                {
                    path = Path.GetDirectoryName(path);
                    break;
                }
            }

            if(string.IsNullOrEmpty(path))
            {
                path = Application.dataPath;
            }
            else
            {
                path = FileSystemUtilities.AssetToFilePath(path);
            }   

            Log.Info("Utils", "Folder path is: " + path);
            return path;
        }

        public static void EscapeSpecialCharacters(string fileExtension, string rootFolderFilepath, string specialCharacter, string unescapeCharacter, string opnameLabel)
        {
            string baseMessage = "Run - " + opnameLabel + " on " + fileExtension + " files, '" + specialCharacter + "' will be replaced with '" + unescapeCharacter + "' ...";          

            // Get files
            List<string> fbxFiles = FileSystemUtilities.FindFilesInDirectories(rootFolderFilepath, fileExtension, true);
            List<string> fbxMetaFiles = FileSystemUtilities.FindFilesInDirectories(rootFolderFilepath, fileExtension + ".meta", true);

            // update the corresponding meta files too!
            fbxFiles.AddRange(fbxMetaFiles);

            EscapeSpecialCharactersInternal(fbxFiles, specialCharacter, unescapeCharacter, opnameLabel, fileExtension, baseMessage);
        }

        public static void EscapeSpecialCharacters(string fileExtension, List<string> filepaths, string specialCharacter, string unescapeCharacter, string opnameLabel)
        {
            string baseMessage = "Run - " + opnameLabel + " on " + fileExtension + " files, '" + specialCharacter + "' will be replaced with '" + unescapeCharacter + "' ...";          

            EscapeSpecialCharactersInternal(filepaths, specialCharacter, unescapeCharacter, opnameLabel, fileExtension, baseMessage);
        }

        private static void EscapeSpecialCharactersInternal(List<string> filepaths, string specialCharacter, string unescapeCharacter, string opnameLabel, string fileExtentionLabel, string baseMessage)
        {
            EditorUtility.DisplayProgressBar(opnameLabel + " Special Characters", baseMessage, 0);

            int count = filepaths.Count;
            if (count == 0)
            {
                EditorUtility.ClearProgressBar();
                return;
            }

            // Replace characters in all of the files
            for (int i = 0; i < count; i++)
            {
                float progress = i / (count * 100);
                EditorUtility.DisplayProgressBar(opnameLabel + " Special Characters", baseMessage + "\n\nCurrent Progress: " + progress + " / " + filepaths.Count + " fileExtentionLabel files", progress);

                System.IO.File.Move(filepaths[i], filepaths[i].Replace(specialCharacter, unescapeCharacter));
            }

            // finished
            EditorUtility.ClearProgressBar();
            Log.Info("Utils", "You have " + opnameLabel + "d!\n\n" + count + " " + fileExtentionLabel + " files, '" + specialCharacter + "' will be replaced with '" + unescapeCharacter + "'");
        }

        [MenuItem("HGC/Tools/Project/Remove Empty Directories")]
        [UsedImplicitly]
        public static void RemoveEmptyDirectories()
        {
            EditorUtility.DisplayProgressBar("Delete Empty Directories", "Finding empty folders", 0);
            List<DirectoryInfo> emptyDirs;
            FillEmptyDirList(out emptyDirs);

            EditorUtility.DisplayProgressBar("Delete Empty Directories", "Deleting " + (emptyDirs != null ? emptyDirs.Count : 0) + " folders", 0.5f);
            if (emptyDirs != null && emptyDirs.Count > 0)
            {
                DeleteAllEmptyDirAndMeta(ref emptyDirs);

                Log.Info("Utils", "Cleaned Empty Directories");
            }
            EditorUtility.ClearProgressBar();
        }

        

        [MenuItem("HGC/Tools/Project/Remove Other Platform Files")]
        [UsedImplicitly]
        public static void RemoveOtherPlatformFiles()
        {
            FileSystemUtilities.RemoveMacDirectoryFiles();
        }

        public static void DeleteAllEmptyDirAndMeta(ref List<DirectoryInfo> emptyDirs)
        {
            var total = emptyDirs.Count;
            for (int index = 0; index < total; index++)
            {
                var dirInfo = emptyDirs[index];

                var path = GetRelativePathFromCd(dirInfo.FullName);

                Log.Info("Utils", "Deleting " + path);
                var cancel = EditorUtility.DisplayCancelableProgressBar("Delete Empty Directories", "Deleting " + path, index / (float)total);
                if (cancel) break;

                Directory.Delete(path);
                File.Delete(path + ".meta");
            }
        }

        public static void FillEmptyDirList(out List<DirectoryInfo> emptyDirs)
        {
            var newEmptyDirs = new List<DirectoryInfo>();
            emptyDirs = newEmptyDirs;

            var assetDir = new DirectoryInfo(Application.dataPath);

            WalkDirectoryTree(assetDir, (dirInfo, areSubDirsEmpty) =>
            {
                bool isDirEmpty = areSubDirsEmpty && DirHasNoFile(dirInfo);
                if (isDirEmpty)
                    newEmptyDirs.Add(dirInfo);
                return isDirEmpty;
            });
        }

        // return: Is this directory empty?
        private delegate bool IsEmptyDirectory(DirectoryInfo dirInfo, bool areSubDirsEmpty);

        // return: Is this directory empty?
        private static bool WalkDirectoryTree(DirectoryInfo root, IsEmptyDirectory pred)
        {
            DirectoryInfo[] subDirs = root.GetDirectories();

            bool areSubDirsEmpty = true;
            var total = subDirs.Length;

            for (int index = 0; index < total; index++)
            {
                var dirInfo = subDirs[index];
                if (false == WalkDirectoryTree(dirInfo, pred))
                {
                    areSubDirsEmpty = false;
                }
            }

            bool isRootEmpty = pred(root, areSubDirsEmpty);
            return isRootEmpty;
        }

        private static bool DirHasNoFile(DirectoryInfo dirInfo)
        {
            FileInfo[] files = null;

            try
            {
                files = dirInfo.GetFiles("*.*");
                files = files.Where(x => !FileSystemUtilities.IsMetaFile(x.Name)).ToArray();
            }
            catch (System.Exception ex)
            {
                Debug.LogException(ex);
            }

            return files == null || files.Length == 0;
        }

        public static string GetRelativePathFromCd(string filespec)
        {
            return GetRelativePath(filespec, Directory.GetCurrentDirectory());
        }

        public static string GetRelativePath(string filespec, string folder)
        {
            System.Uri pathUri = new System.Uri(filespec);

            // Folders must end in a slash
            if (!folder.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                folder += Path.DirectorySeparatorChar;
            }

            System.Uri folderUri = new System.Uri(folder);

            return System.Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', Path.DirectorySeparatorChar));
        }
    }
}