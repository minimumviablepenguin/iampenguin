﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HGC;
using HGC.Scenes;
using HGC.Utils;
using HGC.Storage;

[CustomEditor(typeof(SceneRoot))]
public class SceneInspector : AbstractCustomPropertyInspector
{
    protected override void OnInitialise()
    {
        Log.Info("Scene", "Setting up scene list drawerer");
        CustomPropertyInspectors.AddOrUpdate("dependencies", new ListPropertyDrawer("dependencies"));
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var root = target as SceneRoot;

        UpdateSceneName(root);
    }
    
    public void Reset()
    {
        if (target == null)
            return;

        Log.Info("Resetting...");

        var sceneRoot = target as SceneRoot;
        UpdateSceneName(sceneRoot);
    }

    private void UpdateSceneName(SceneRoot sceneRoot)
    {
        // TODO - apis are lower because of vive sdk
        /*
        var property = serializedObject.FindProperty("_sceneName");
        var name = property.stringValue;
        string activeScene = EditorSceneManager.GetActiveScene().name;

        // update scene name if needed
        if (string.IsNullOrEmpty(name))
        {
            Log.Info("Scene", "Updating Scene Root's name to: " + activeScene);
            property.stringValue = EditorSceneManager.GetActiveScene().name;

            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();

            // update root game object name
            if (sceneRoot.gameObject.name != property.stringValue)
                sceneRoot.gameObject.name = property.stringValue;
        }
        */    
    }   
}
