﻿using UnityEditor;
using HGC.Utils;

namespace HGC.Scenes
{
    public class BuildSettingsEditor
    {
        [MenuItem("HGC/Tools/Build/Add All Scenes")]
        public static void AddAllScenes()
        {
            Log.Info("Build", "Adding scenes to build located in scene folder: " + SceneAttribute.SceneFolderFilepath);

            var sceneFilePaths = FileSystemUtilities.FindFilesInDirectories(SceneAttribute.SceneFolderFilepath, ".unity");
            var newSettings = new EditorBuildSettingsScene[sceneFilePaths.Count];

            try
            {
                for (int i=0; i <sceneFilePaths.Count; i++)
                {
                    newSettings[i] = new EditorBuildSettingsScene(FileSystemUtilities.FileToAssetPath(sceneFilePaths[i]), true);
                }
            }
            catch(System.Exception e)
            {
                Log.Error("Build", "Failed to add scenes to build: " + e.Message + "\n\n" + e.StackTrace);
                return;
            }

            if (newSettings.Length != 0)
                Log.Info("Build", "Added " + newSettings.Length + " Scenes");
            else
                Log.Error("Build", "Did not find any scenes in the scene folder specified in Editor Settings: " + SceneAttribute.SceneFolderFilepath);

            EditorBuildSettings.scenes = newSettings;
        }
    }
}
