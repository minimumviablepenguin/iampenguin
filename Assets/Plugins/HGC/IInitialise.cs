﻿namespace HGC
{
    public interface IInitialise
    {
        bool IsInitialised();
        void OnInitialise();
    }
}
