using System.Collections.Generic;

public class ObjectPool<T> where T : class, IReleaseable, new()
{
    private List<T> _free = new List<T>();
    private List<T> _used = new List<T>();
	private bool _preallocated = false;
	
	public int ActiveCount { get { return _used.Count; } }
	public int InactiveCount { get { return _free.Count; } } 

	public ObjectPool(int size = 1)
	{
		PreAllocate(size);
	} 

	private void PreAllocate(int preAllocationSize=1)
	{	
		// pre-allocating moves the instantiation overhead to when the pool is instantiated. In contrast to lazy initialisation e.g. a Singleton.Instance().
		if(_preallocated)
		{
			throw new System.InvalidOperationException("Object pool was preallocated twice. This should only be called once, immediately after instantiating the pool");	
		}
		
		if(preAllocationSize<1)
			preAllocationSize=1;
		
		for(int i=0; i<preAllocationSize; i++)
		{
			_free.Add(new T());
		}
		
		_preallocated = true;
	}
	
    public T Get()
    {
        T pooled = null;

        if (_free.Count == 0)
        {
			pooled = new T();
        }
        else
        {
            pooled = _free[_free.Count - 1];
            _free.RemoveAt(_free.Count - 1);
        }

        _used.Add(pooled);

        return pooled;
    }

    public void Free(T toFreeObject)
    {
		toFreeObject.Release();
        _free.Add(toFreeObject);
        _used.Remove(toFreeObject);
    }

    public void FreeAll()
    {
        for (int i = 0; i < _used.Count; ++i)
        {
            _free.Add(_used[i]);
        }

        _used.Clear();
    }
}