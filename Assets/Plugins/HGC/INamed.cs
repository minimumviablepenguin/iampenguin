﻿using UnityEngine;
using System.Collections;

namespace HGC.Storage
{
	public interface INamed
	{
		string Name
		{
			set;
			get;
		}
	}
}

