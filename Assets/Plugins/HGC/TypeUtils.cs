﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Reflection;

namespace HGC.Utils
{
    public static class TypeUtil
    {
        /// <summary>
        /// Gets the type of an unqualified typename. Returns the first match.
        /// This will use file system operations to load types from non-unity assemblies.
        /// </summary>
        /// <param name="unqualifiedTypename"></param>
        /// <returns>fullyQualifiedTypenmae</returns>
        public static System.Type GetTypeFromAssemblies(string unqualifiedTypename)
        {
            return Type.GetType(GetQualifiedTypeNameFromAssemblies(unqualifiedTypename));
        }

        /// <summary>
        /// Finds the fully qualified typename of an unqualified type. Returns the first match.
        /// This will use file system operations to load types from non-unity assemblies.
        /// </summary>
        /// <param name="unqualifiedTypename"></param>
        /// <returns>fullyQualifiedTypenmae</returns>
        public static string GetQualifiedTypeNameFromAssemblies(string unqualifiedTypename)
        {
            // Find type in current assemblies...
            foreach (var assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in assembly.GetTypes())
                {
                    if (type.Name.EndsWith(unqualifiedTypename))
                        return type.AssemblyQualifiedName;
                }
            }

            // Find type in external assemblies...
            string pluginsFolder = Application.dataPath + "/Plugins";
            List<string> filePaths = FileSystemUtilities.FindFilesInDirectories(pluginsFolder, ".dll");

            foreach (string dllPath in filePaths)
            {
                var dll = Assembly.LoadFile(dllPath);

                if (dll == null)
                    continue;

                foreach (Type type in dll.GetExportedTypes())
                {
                    if (type.Name.EndsWith(unqualifiedTypename))
                        return type.AssemblyQualifiedName;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Returns all instances of type T in array
        /// </summary>
        /// <typeparam name="T">Interface type</typeparam>
        /// <returns></returns>
        public static T[] FindInterfacesFromAllGameObjects<T>(GameObject gObj, params string[] ignoreTags)
        {
            GameObject[] gObjects = UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];

            List<T> scriptList = new List<T>();
            foreach (GameObject gObject in gObjects)
            {
                bool shouldIgnoreObject = false;
                foreach(var tag in ignoreTags)
                {
                    if (gObject.CompareTag(tag))
                    {
                        shouldIgnoreObject = true;
                        break;
                    }
                }

                if (shouldIgnoreObject)
                    continue;

                T[] scripts = GetInterfaces<T>(gObject);
                foreach (T script in scripts)
                { 
                    if (!scriptList.Contains(script))
                        scriptList.Add(script);
                }
            }
            return scriptList.ToArray();
        }


        /// <summary>
        /// Returns all instances of type T in array
        /// </summary>
        /// <typeparam name="T">Interface type</typeparam>
        /// <returns></returns>
        public static T[] FindInterfacesFromAllGameObjects<T>()
        {
            GameObject[] gObjects = UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];

            List<T> scriptList = new List<T>();
            foreach (GameObject gObject in gObjects)
            {
                T[] scripts = GetInterfaces<T>(gObject);
                foreach (T script in scripts)
                {
                    if (!scriptList.Contains(script))
                        scriptList.Add(script);
                }
            }
            return scriptList.ToArray();
        }


        /// <summary>
        /// Returns array with all gameobjects that contains at least 1 of type T interface, with no duplicates
        /// </summary>
        /// <typeparam name="T">Interface type</typeparam>
        /// <returns></returns>
        public static GameObject[] FindUniqueGameObjectsWithInterface<T>(GameObject gObj)
        {
            GameObject[] gObjects = UnityEngine.Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];

            List<GameObject> uniqueObjects = new List<GameObject>();
            foreach (GameObject gObject in gObjects)
            {
                T script = GetInterface<T>(gObject);
                //Check for non-null && non-default value (not quite sure on what default does)
                if (!object.Equals(script, default(T)))
                {
                    if (!uniqueObjects.Contains(gObject))
                        uniqueObjects.Add(gObject);
                }
            }
            return uniqueObjects.ToArray();
        }

        /// <summary>
        /// Returns all monobehaviours (casted to T)
        /// </summary>
        /// <typeparam name="T">interface type</typeparam>
        /// <param name="gObj"></param>
        /// <returns></returns>
        public static T[] GetInterfaces<T>(GameObject gObj)
        {
            if (!typeof(T).IsInterface)
                throw new System.SystemException("Specified type is not an interface!");
            var mObjs = gObj.GetComponents<MonoBehaviour>();

            List<T> tList = new List<T>();
            foreach (MonoBehaviour tA in mObjs)
            {
                if (tA == null)
                {
                    Debug.LogError(gObj + " has a null monobehavior component", gObj);
                    continue;
                }
                if (tA.GetType().GetInterfaces().Any(k => k == typeof(T)))
                    tList.Add((T)(object)tA);
            }
            return tList.ToArray();

        }

        /// <summary>
        /// Returns the first monobehaviour that is of the interface type (casted to T)
        /// </summary>
        /// <typeparam name="T">Interface type</typeparam>
        /// <param name="gObj"></param>
        /// <returns></returns>
        public static T GetInterface<T>(GameObject gObj)
        {
            if (!typeof(T).IsInterface) throw new System.SystemException("Specified type is not an interface!");
            return GetInterfaces<T>(gObj).FirstOrDefault();
        }

        /// <summary>
        /// Returns the first instance of the monobehaviour that is of the interface type T (casted to T)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gObj"></param>
        /// <returns></returns>
        public static T GetInterfaceInChildren<T>(GameObject gObj)
        {
            if (!typeof(T).IsInterface) throw new System.SystemException("Specified type is not an interface!");
            return GetInterfacesInChildren<T>(gObj).FirstOrDefault();
        }

        /// <summary>
        /// Gets all monobehaviours in children that implement the interface of type T (casted to T)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gObj"></param>
        /// <returns></returns>
        public static T[] GetInterfacesInChildren<T>(GameObject gObj)
        {
            if (!typeof(T).IsInterface) throw new System.SystemException("Specified type is not an interface!");
            var mObjs = gObj.GetComponentsInChildren<MonoBehaviour>();
            return (from a in mObjs where a.GetType().GetInterfaces().Any(k => k == typeof(T)) select (T)(object)a).ToArray();
        }

        public static IEnumerable<Type> GetTypes(System.Func<System.Type, bool> predicate)
        {
            foreach (var assemb in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var tp in assemb.GetTypes())
                {
                    if (predicate(tp)) yield return tp;
                }
            }
        }

        public static IEnumerable<Type> GetTypesAssignableFrom(System.Type rootType)
        {
            foreach (var assemb in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var tp in assemb.GetTypes())
                {
                    if (rootType.IsAssignableFrom(tp)) yield return tp;
                }
            }
        }

        public static IEnumerable<Type> GetTypesAssignableFrom(System.Reflection.Assembly assemb, System.Type rootType)
        {
            foreach (var tp in assemb.GetTypes())
            {
                if (rootType.IsAssignableFrom(tp) && rootType != tp) yield return tp;
            }
        }

        public static bool IsType(System.Type tp, System.Type assignableType)
        {
            return assignableType.IsAssignableFrom(tp);
        }

        public static bool IsType(System.Type tp, params System.Type[] assignableTypes)
        {
            foreach (var otp in assignableTypes)
            {
                if (otp.IsAssignableFrom(tp)) return true;
            }

            return false;
        }

        public static object GetDefaultValue(this System.Type tp)
        {
            if (tp == null) throw new System.ArgumentNullException("tp");

            if (tp.IsValueType)
                return System.Activator.CreateInstance(tp);
            else
                return null;
        }


        public static System.Type ParseType(string assembName, string typeName)
        {
            var assemb = (from a in System.AppDomain.CurrentDomain.GetAssemblies()
                            where a.GetName().Name == assembName || a.FullName == assembName
                            select a).FirstOrDefault();
            if (assemb != null)
            {
                return (from t in assemb.GetTypes()
                        where t.FullName == typeName
                        select t).FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public static System.Type FindType(string typeName, bool useFullName = false, bool ignoreCase = false)
        {
            if (string.IsNullOrEmpty(typeName)) return null;

            StringComparison e = (ignoreCase) ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal;
            if (useFullName)
            {
                foreach (var assemb in System.AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (var t in assemb.GetTypes())
                    {
                        if (string.Equals(t.FullName, typeName, e)) return t;
                    }
                }
            }
            else
            {
                foreach (var assemb in System.AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (var t in assemb.GetTypes())
                    {
                        if (string.Equals(t.Name, typeName, e) || string.Equals(t.FullName, typeName, e)) return t;
                    }
                }
            }
            return null;
        }

        public static System.Type FindType(string typeName, System.Type baseType, bool useFullName = false, bool ignoreCase = false)
        {
            if (string.IsNullOrEmpty(typeName)) return null;
            if (baseType == null) throw new System.ArgumentNullException("baseType");

            StringComparison e = (ignoreCase) ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal;
            if (useFullName)
            {
                foreach (var assemb in System.AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (var t in assemb.GetTypes())
                    {
                        if (baseType.IsAssignableFrom(t) && string.Equals(t.FullName, typeName, e)) return t;
                    }
                }
            }
            else
            {
                foreach (var assemb in System.AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (var t in assemb.GetTypes())
                    {
                        if (baseType.IsAssignableFrom(t) && (string.Equals(t.Name, typeName, e) || string.Equals(t.FullName, typeName, e))) return t;
                    }
                }
            }

            return null;
        }

        public static bool IsListType(this System.Type tp)
        {
            if (tp == null) return false;

            if (tp.IsArray) return tp.GetArrayRank() == 1;

            var interfaces = tp.GetInterfaces();
            if (interfaces.Contains(typeof(System.Collections.IList)) || interfaces.Contains(typeof(IList<>)))
            {
                return true;
            }

            return false;
        }

        public static bool IsListType(this System.Type tp, bool ignoreAsInterface)
        {
            if (tp == null) return false;

            if (tp.IsArray) return tp.GetArrayRank() == 1;

            if (ignoreAsInterface)
            {
                if (tp.IsGenericType && tp.GetGenericTypeDefinition() == typeof(List<>)) return true;
            }
            else
            {
                var interfaces = tp.GetInterfaces();
                if (interfaces.Contains(typeof(System.Collections.IList)) || interfaces.Contains(typeof(IList<>)))
                {
                    return true;
                }
            }

            return false;
        }

        public static System.Type GetElementTypeOfListType(this System.Type tp)
        {
            if (tp == null) return null;

            if (tp.IsArray) return tp.GetElementType();

            var interfaces = tp.GetInterfaces();
            if (interfaces.Contains(typeof(System.Collections.IList)) || interfaces.Contains(typeof(IList<>)))
            {
                if (tp.IsGenericType) return tp.GetGenericArguments()[0];
                else return typeof(object);
            }

            return null;
        }
    }
}
