﻿using UnityEngine;
using System.Collections;
using System.Reflection;

namespace HGC.CustomEventSystem{
	[System.Serializable]
	public class EventReferenceEntry {
		public GameObject target;
		public string component;
		public string property;
		
		public Component GetComponent(){
			return target.GetComponent(component);
		}
		
		public PropertyInfo GetPropertyInfo(){
			Component comp = GetComponent ();
			if (comp != null) {
				return comp.GetType().GetProperty(property);		
			}
			return null;
		}
		
		public PropertyInfo GetPropertyInfo(Component comp){
			if (comp != null) {
				return comp.GetType().GetProperty(property);		
			}
			return null;
		}
		
		public FieldInfo GetFieldInfo(){
			Component comp = GetComponent ();
			if (comp != null) {
				return comp.GetType().GetField(property);		
			}
			return null;
		}
		
		public FieldInfo GetFieldInfo(Component comp){
			if (comp != null) {
				return comp.GetType().GetField(property);		
			}
			return null;
		}
		
		public bool SetValue(object value){
			Component comp = target.GetComponent (component);
			if(comp != null){
				PropertyInfo prop = GetPropertyInfo (comp);
				if(prop != null){
					prop.SetValue(comp,value,null);	
					return true;
				}
				FieldInfo field = GetFieldInfo (comp);
				if(field != null){
					field.SetValue(comp,value);
				}
			}
			return false;
		}

		public object GetValue(){
			Component comp = target.GetComponent (component);
			if(comp != null){
				PropertyInfo prop = GetPropertyInfo (comp);
				if(prop != null){

					return prop.GetValue(comp,null);
				}
				FieldInfo field = GetFieldInfo (comp);
				if(field != null){
					return field.GetValue(comp);
				}
			}
			return null;
		}
	}
}
