﻿using UnityEngine;
using System.Collections;
using HGC;

public interface ISingleton : IInitialise
{
    void OnShutdown();

    // OTHER, to enforce memory management? Maybe add this to services
    //void OnGameLevelLoad();
    //void OnApplicationQuit();
}
