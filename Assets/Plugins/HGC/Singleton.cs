﻿#pragma warning restore 612, 618

using UnityEngine;
using System.Collections;
using HGC;
using System;
using JetBrains.Annotations;
using System.Diagnostics.CodeAnalysis;

public abstract class Singleton<T> : ManagedBehaviour, ISingleton where T : ManagedBehaviour, ISingleton
{
    [SerializeField]
    protected bool _isPersistent;

    [NonSerialized]
    private bool _isInitialised;

    private static Singleton<T> _instance;
    private static bool _applicationIsQuitting = false;
    private static object _lock = new object();

    public bool IsInitialised() { return _isInitialised; }

#region StaticMethods
    public static bool HasInstance() { return _instance != null; }
    public static bool ValidateInstance()
    {
#if !UNITY_EDITOR
        return true;
#else
        if(HasInstance() && Instance.IsInitialised())
        {
            return true;
        }
        else
        {
            Log.Error("Singleton", "Singleton Instance was not initialised");
            return false;
        }
#endif
    }

    public static T Instance
	{
		get
		{		
			lock(_lock)
			{
                if (_applicationIsQuitting)
                {
                    Debug.LogWarning("[Singleton] Instance (" + typeof(Singleton<T>) + ") was already destroyed on application quit, returning null.");
                    return null;
                }

                if (_instance != null)
                {
                    return _instance as T;
                }
                else
                {
                    _instance = FindObjectOfType(typeof(T)) as Singleton<T>;

                    if(_instance == null)
                    {
                        GameObject singleton = new GameObject();
                        singleton.name = typeof(T).ToString();

                        var component = singleton.AddComponent<T>() as Singleton<T>;
                        component._isPersistent = true;

                        Log.Warn("Singleton", "Singleton (" + typeof(Singleton<T>) + ") is being instantiated (will live forever)");
                        DontDestroyOnLoad(singleton);

                        component.OnInitialise();
                        component._isInitialised = true;

                        return component as T;
                    }

                    return _instance as T;
                }
            }
		}
	}
    #endregion


    #region InstanceMethods

    protected override void Start()
    {
        if (HasInstance() && _instance != this)
        {
            Log.Assert(false, "Singleton", "A singleton (" + this + ") was already created", this);
            Debug.Break();
        }
        else
        {
            _instance = this;
            if (_isPersistent && transform.parent == null)
            {
                DontDestroyOnLoad(this);
            }  

            if (!_isInitialised)
            {
                Log.Info("Singleton", "Initialising singleton (" + this + ")");
                OnInitialise();
                _isInitialised = true;
            }
        }

        // Note singletons do not get automatically get registered to custom message callbacks
        CacheVariablesOnStart();

        OnStart();
    }

    public virtual void OnInitialise()
    {
    }

    public virtual void OnShutdown()
    {
    }

    protected virtual void OnDestroy()
    {
        if (_instance == this)
        {
            _instance = null;
            OnShutdown();
        }
    }

    protected virtual void OnApplicationQuit()
    {
        if (_instance == this)
        {
            _applicationIsQuitting = true;
            _instance = null;
            OnShutdown();
        }
    }
#endregion
}
