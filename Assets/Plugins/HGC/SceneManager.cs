﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Diagnostics;
using JetBrains.Annotations;
using HGC.Events;

namespace HGC
{
    public class SceneManager : Singleton<SceneManager>
    {
        private bool _hasLoaded;

        private List<string> _loadedScenes;

        public static bool IsLoaded(string scene)
        {
            return Instance._loadedScenes.Contains(scene);
        }

        #region StartupCallbacks

        protected override void OnAwake()
        {
            _loadedScenes = new List<string>();
            _hasLoaded = false;
        }

        protected override void OnStart()
        {
            Log.Info("SceneManager", "Scene Manager Started");
        }

        public void Update()
        {
            if(!_hasLoaded)
            {
                if(_loadedScenes == null)
                {
                    MessageManager.Broadcast<GameLoaded>();
                    _hasLoaded = true;
                }
            }
        }

#endregion StartupCallbacks

        public static IEnumerator LoadScenes(IEnumerable<string> scenesToLoad)
        {
            Log.Info("Scene", "Loading " + scenesToLoad.ToList().Count + " scenes...");
            var index = 0;
            foreach (var scene in scenesToLoad)
            {
                if (Instance._loadedScenes.Contains(scene))
                {
                    Log.Warn("Scene", "Scene dependency '" + scene + "' is already loaded, skipping...");
                    continue;
                }

                Log.Info("Scene", "Loading scene: " + scene + "...");

                var num = LoadScene(scene);
                while (num.MoveNext())
                {
                    yield return num.Current;
                }
                index++;
            }
        }

        public static IEnumerator LoadScene(string scene)
        {
            if(Instance._loadedScenes.Contains(scene))
                yield break;

            Log.Info("Scene", "Loading scene: " + scene + "...");
            Instance._loadedScenes.Add(scene);

            var op = Application.LoadLevelAdditiveAsync(scene);

            while (op.isDone == false)
            {
                yield return null;
            }
        }
    }
}
