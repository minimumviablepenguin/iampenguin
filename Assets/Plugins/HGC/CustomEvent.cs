﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace HGC.CustomEventSystem{
	[System.Serializable]
	public class CustomEvent {
		public string method;
		[SerializeField]
		private string type;
		public Type SerializedType{
			get{
				string[] split=type.Split('.');
				return Type.GetType(type+(split[0]=="UnityEngine"?",UnityEngine":""));	
			}
			set{
				type=value.ToString();
			}
		}
		public int selectedOverloadMethodIndex;
		public List<EventParameter> parameters;
		public EventReferenceEntry entry;
		
		private bool cached = false;
		private MethodInfo methodInfo;
		private object[] args;
		private Component component;
		
		
		public CustomEvent(string type,string method){
			this.type = type;
			this.method = method;
		}
		
		public CustomEvent(Type type,string method){
			this.SerializedType = type;
			this.method = method;
		}
		
		public void Execute(GameObject target){
			if (!cached) {
				cached=true;	
				List<Type> paramTypes=new List<Type>();
				List<object> arguments= new List<object>();
				
				foreach(EventParameter p in parameters){
					paramTypes.Add(p.SerializedType);
					arguments.Add(p.Get());
				}
				args=arguments.ToArray();
				methodInfo=SerializedType.GetMethod(method,paramTypes.ToArray());
				component=target.GetComponent(SerializedType);
			}
			
			try{
				object result=methodInfo.Invoke (component, args);
				if(result != null && entry.target != null){
					entry.SetValue(result);
				}
			}catch{
				
			}
		}
	}
}
