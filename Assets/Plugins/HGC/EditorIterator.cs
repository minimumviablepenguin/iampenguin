using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HGC
{
    /// <summary>
    /// Runs a coroutine in the UnityEditor. If the application is not being run in the editor.
    /// </summary>
    public class EditorIterator
    {
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void Start(IEnumerator _routine)
        {
#if UNITY_EDITOR
            EditorIterator coroutine = new EditorIterator(_routine);
            coroutine.Start();
#else
            Log.Warn("Editor", "Cannot run an EditorCoroutine outside of the editor. Start will return null");
#endif
        }

        readonly IEnumerator routine;
        EditorIterator(IEnumerator _routine)
        {
            routine = _routine;
        }

        void Start()
        {
#if UNITY_EDITOR
            EditorApplication.update += Update;
#endif
        }
        public void Stop()
        {
#if UNITY_EDITOR
            EditorApplication.update -= Update;
#endif
        }

        void Update()
        {
            if (!routine.MoveNext())
            {
                Stop();
            }
        }
    }
}
