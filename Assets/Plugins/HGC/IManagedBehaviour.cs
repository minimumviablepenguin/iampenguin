﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace HGC
{
    /// <summary>
    /// Contains cached variables for extended monobehaviours and their extension methods
    /// </summary>
    public interface IManagedBehaviour
    {
        string Tag { get; }
        string TypeName { get; }
        GameObject GameObject { get; }
        Transform Transform { get; }
        RectTransform RectTransform { get; }
        string Name { get; }
    }
}
