﻿using UnityEngine;
using System.Collections;

public class Event_Randall_Intro : MonoBehaviour
{
	public PenguinAgent Bob;
	public PenguinAgent Rob;
	public GameObject bobButton;

	public PenguinAgent currentShouter;


	// Update is called once per frame
	void Update()
	{
		if (Conversation.inConversation)
		{
			bobButton.GetComponent<UILabel>().enabled = false;
		}
		else
		{
			bobButton.GetComponent<UILabel>().enabled = true;
		}

		if (bobButton.activeInHierarchy && currentShouter != null)
		{
			if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Joystick1Button0))
			{
//				AssetManager.Player.GetComponent<PenguinController>().RandomAudioShout();
//				currentShouter.PlayRandomShoutSound(1.0f);
			}
		}
	}

	public void PointToBob()
	{
		transform.parent.parent.rotation = Quaternion.LookRotation(Bob.transform.position - transform.parent.parent.position);
		transform.parent.parent.GetComponent<PenguinAgent>().Point();

		foreach (var penguinAgent in PenguinAgent.allPenguins)
		{
			penguinAgent.generateRandomPath = true;
			if (penguinAgent.name == "Rob" || penguinAgent.name == "Bob")
				penguinAgent.generateRandomPath = false;
		}

		bobButton.SetActive(true);
		currentShouter = Rob;
	}

	public void BobAnswers()
	{
		currentShouter = Bob;
	}

	public void RemoveBobButton()
	{
		bobButton.SetActive(false);
	}
}
