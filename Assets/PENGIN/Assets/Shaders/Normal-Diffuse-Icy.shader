Shader "Diffuse Icy" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_IceTex ("Base (RGB)", 2D) = "white" {}
		_IceAmount ("Amount Of Icyness", Range(0.0,1.0)) = 0
		_Cube ("Reflection Cubemap", Cube) = "_Skybox" { TexGen CubeReflect }

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf BlinnPhong

		sampler2D _MainTex;
		sampler2D _IceTex;
		samplerCUBE _Cube;
		float4 _Color;
		float _IceAmount;


		struct Input {
			float2 uv_MainTex;
			float2 uv_IceTex;
            float3 worldNormal; 
			float3 worldRefl;
		};


		void surf (Input IN, inout SurfaceOutput o) 
		{	

			_IceAmount = clamp(_IceAmount, 0.1, 1);
			float _Pow = 1.75;
			float3 world_normal = saturate(IN.worldNormal);
			float4 main_texture = lerp(tex2D(_MainTex, IN.uv_MainTex), _Color, world_normal.yyyy * _Color.a);

			float4 reflcol = texCUBE (_Cube, IN.worldRefl);


			float4 ice_texture = tex2D(_IceTex, IN.uv_IceTex);

			o.Emission = reflcol * main_texture.a;
			o.Albedo = lerp(main_texture, ice_texture,  saturate(( 0.5 + pow(world_normal.yyyy * _Pow, _Pow)) * _IceAmount * pow(Luminance(ice_texture), 2)));
		}
		ENDCG
	}
Fallback Off
}


