﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
//using Fabric;

public class PenguinAgent : MonoBehaviour
{
	[System.Serializable]
	public class PenguinRelation
	{
		public PenguinAgent penguin;
		public float distanceWanted = 1.0f;
	}
	
	public bool generateRandomPath = false;
	public float generateRandomPathDelayMin = 3.0f;
	public float generateRandomPathDelayMax = 6.0f;
	public float generateRandomPathRadius = 2.0f;
	public float minDistanceBeforeDie = 8.0f;
	public float health = 100.0f;
	public float damageWhenAloneAmplitude = 1.0f;
	public NavMeshAgent agent;
/*	public RandomComponent audioRandomAngry;
	public RandomComponent audioRandomChatter;
	public RandomComponent audioRandomQuestion;
	public RandomComponent audioRandomSad;
	public RandomComponent audioRandomShout;
	public RandomComponent audioRandomDie;
	public RandomComponent audioRandomWalking;
	public AudioSource audioDie;
*/
	[SerializeField] public List<PenguinRelation> penguinRelations = new List<PenguinRelation>();

	private float generateNextRandomPathTime = 0.0f;
	private Vector3 wantedPosition = Vector3.zero;
	private Vector3 recalculatedWantedVector = Vector3.zero;
	public bool dead = false;
	private bool busy = false;

	public bool drawDebug = false;

	public static List<PenguinAgent> allPenguins = new List<PenguinAgent>();

	private float startHealth;
	private SkinnedMeshRenderer meshRenderer;
	private SkinnedMeshRenderer iceMeshRenderer;

	private Animator m_Animator = null;
    
    private int m_SpeedId = 0;
    private int m_AgularSpeedId = 0;
    private int m_DirectionId = 0;

    private float m_SpeedDampTime = 0.1f;
    private float m_AnguarSpeedDampTime = 0.25f;
    private float m_DirectionResponseTime = 0.2f;

    public void Do(float speed, float direction)
    {
        AnimatorStateInfo state = m_Animator.GetCurrentAnimatorStateInfo(0);

        bool inTransition = m_Animator.IsInTransition(0);
        bool inIdle = state.IsName("Locomotion.peng_idle");
        bool inTurn = state.IsName("Locomotion.peng_turn_right");
        bool inRun = state.IsName("Locomotion.peng_walk_forward");

        float speedDampTime = inIdle ? 0 : m_SpeedDampTime;
        float angularSpeedDampTime = inRun || inTransition ? m_AnguarSpeedDampTime : 0;
        float directionDampTime = inTurn || inTransition ? 1000000 : 0;

        float angularSpeed = direction / m_DirectionResponseTime;

        m_Animator.SetFloat(m_SpeedId, speed, speedDampTime, Time.deltaTime);
	    //m_Animator.SetFloat(m_AgularSpeedId, angularSpeed, angularSpeedDampTime, Time.deltaTime);
	    //m_Animator.SetFloat(m_DirectionId, direction, directionDampTime, Time.deltaTime);
	}	


	// Use this for initialization
	void Start()
	{
		allPenguins.Add(this);
		agent = GetComponent<NavMeshAgent>();
		agent.updateRotation = false;

		m_Animator = GetComponentInChildren<Animator>();

		m_SpeedId = Animator.StringToHash("Speed");
		m_AgularSpeedId = Animator.StringToHash("AngularSpeed");
		m_DirectionId = Animator.StringToHash("Direction");

		meshRenderer = transform.FindChild("Penguin").GetComponent<SkinnedMeshRenderer>();
		iceMeshRenderer = transform.FindChild("Ice_Crystal_101").GetComponent<SkinnedMeshRenderer>();
		iceMeshRenderer.enabled = false;
		startHealth = health;
		float iceFactor = (100.0f - health) * 0.01f;
		meshRenderer.material.SetFloat("_IceAmount", iceFactor);

		generateRandomPath = true;
		wantedPosition = transform.position;
	}

	// Update is called once per frame
	void Update()
	{
		if (dead || busy)
			return;

		if (health < 0)
		{
			Die();
			return;
		}

		if (health < startHealth)
		{
			float iceFactor = (100.0f - health) * 0.01f;
			meshRenderer.material.SetFloat("_IceAmount", iceFactor);
		}

		CalculateRelations();

		if (AgentDone())
		{
			Do(0, 0);
//			audioRandomWalking.Stop();
		}
		else
		{
//			audioRandomWalking.Play(audioRandomWalking.CreateInstance(audioRandomWalking.gameObject));
			float speed = agent.desiredVelocity.magnitude;
			Vector3 velocity = Quaternion.Inverse(transform.rotation) * agent.desiredVelocity;
			float angle = Mathf.Atan2(velocity.x, velocity.z) * 180.0f / 3.14159f;
			Do(speed, angle);
			transform.rotation = Quaternion.Euler(velocity);
		}
	}

	void LateUpdate()
	{
		if (dead || busy)
			return;

		if (recalculatedWantedVector != Vector3.zero)
		{
			wantedPosition = transform.position + recalculatedWantedVector;
			recalculatedWantedVector = Vector3.zero;
		}

		if (generateRandomPath)
		{
			if (Time.time > generateNextRandomPathTime)
			{
				SetDestination();
			}
		}

		if (drawDebug)
		{
			Debug.DrawLine(transform.position, wantedPosition, Color.green);
		}
	}

	void OnAnimatorMove()
	{
		agent.velocity = m_Animator.deltaPosition / Time.deltaTime;
		//transform.rotation = m_Animator.rootRotation;
	}

	private void CalculateRelations()
	{
		var closestPenguin = Mathf.Infinity;
		var lastPenguin = true;

		foreach (var penguinRelation in penguinRelations)
		{
			if (penguinRelation.penguin == null)
				continue;

			var distanceBetween = Vector3.Distance(transform.position, penguinRelation.penguin.transform.position);
			var wantedDistance = penguinRelation.distanceWanted - distanceBetween;

			recalculatedWantedVector += (transform.position - penguinRelation.penguin.transform.position).normalized * wantedDistance;
		}

		foreach (var penguinAgent in allPenguins)
		{
			if (penguinAgent == this || penguinAgent.dead)
				continue;

			var distanceBetween = Vector3.Distance(transform.position, penguinAgent.transform.position);
			if (distanceBetween < closestPenguin)
			{
				closestPenguin = distanceBetween;
			}
			if (!penguinAgent.dead)
			{
				lastPenguin = false;
			}
		}

		if (lastPenguin || closestPenguin > minDistanceBeforeDie)
		{
			health -= Time.deltaTime*damageWhenAloneAmplitude;
		}
		else
		{
			health = Mathf.Clamp(health, 0.0f, startHealth);
		}
	}

	protected void SetDestination()
	{
		var insideUnitCircle = Random.insideUnitCircle * generateRandomPathRadius;
		var newPos = wantedPosition + new Vector3(insideUnitCircle.x, 0.0f, insideUnitCircle.y);
		agent.destination = newPos;

		generateNextRandomPathTime = Time.time + Random.Range(generateRandomPathDelayMin, generateRandomPathDelayMax);
	}

	protected bool AgentDone()
	{
		return !agent.pathPending && AgentStopping();
	}

	protected bool AgentStopping()
	{
		return agent.remainingDistance <= agent.stoppingDistance;
	}

#if UNITY_EDITOR
    void OnDrawGizmosSelected()
	{
		if (dead)
			return;

		foreach (var penguinRelation in penguinRelations)
		{
			penguinRelation.distanceWanted = UnityEditor.Handles.RadiusHandle(Quaternion.identity,
			                                                      Vector3.Lerp(transform.position,
			                                                                   penguinRelation.penguin.transform.position, 0.5f),
			                                                      penguinRelation.distanceWanted*0.5f) * 2.0f;

            UnityEditor.Handles.Label(Vector3.Lerp(transform.position, penguinRelation.penguin.transform.position, 0.5f), penguinRelation.distanceWanted.ToString("N1"));
            UnityEditor.Handles.DrawLine(transform.position, penguinRelation.penguin.transform.position);
		}

        UnityEditor.Handles.Label(transform.position + Vector3.up, transform.name);
	}
#endif

    private void Die()
	{
//		audioDie.Play();

//		audioRandomDie.Play(audioRandomDie.CreateInstance(audioRandomDie.gameObject));
		Instantiate(Resources.Load("Ice Crystals"), transform.position, transform.rotation);
		allPenguins.Remove(this);
		Do(0, 0);
		m_Animator.speed = 0;
		agent.Stop(true);
		dead = true;
		iceMeshRenderer.enabled = true;
	}

	public void StartConversationActions()
	{
		busy = true;
		agent.Stop(true);
		transform.LookAt(AssetManager.Player.transform);
	}

	public void StopConversationActions()
	{
		busy = false;
	}

	public void HappyPenguin()
	{
		
	}

	public void SadPenguin()
	{
		foreach (var penguinRelation in penguinRelations)
		{
			penguinRelation.distanceWanted = 15.0f;
		}
	}

	public void HitPenguin(float hit)
	{
		health -= hit;
	}

	public void Point()
	{
		m_Animator.SetTrigger("Action_Point");
	}

	public void PlayRandomAngrySound()
	{
//		audioRandomAngry.Play(audioRandomDie.CreateInstance(audioRandomDie.gameObject));
	}
	public void PlayRandomChatterSound()
	{
//		audioRandomChatter.Play(audioRandomChatter.CreateInstance(audioRandomChatter.gameObject));
	}
	public void PlayRandomQuestionSound()
	{
//		audioRandomQuestion.Play(audioRandomQuestion.CreateInstance(audioRandomQuestion.gameObject));
	}
	public void PlayRandomSadSound()
	{
//		audioRandomSad.Play(audioRandomSad.CreateInstance(audioRandomSad.gameObject));
	}
	public void PlayRandomShoutSound(float delay = 0.0f)
	{
//		StartCoroutine(PlayRandomSoundDelayed(audioRandomShout, delay));
	}

/*	private IEnumerator PlayRandomSoundDelayed(RandomComponent sound, float delay)
	{
		yield return new WaitForSeconds(delay);
		sound.Play(sound.CreateInstance(sound.gameObject));
	}*/
}
