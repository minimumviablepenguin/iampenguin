using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("Image Effects/Krillbite/Icy Overlay")]

public class IcyOverlay : ImageEffectBase
{
	[SerializeField] private Texture overlayTex;
	[SerializeField][Range(0.0f, 1.0f)] public float overlayIntensity = 1.0f;


	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		material.SetFloat("overlayIntensity", overlayIntensity);
		material.SetTexture("_OverlayTex", overlayTex);
		Graphics.Blit(source, destination, material);
	}
}