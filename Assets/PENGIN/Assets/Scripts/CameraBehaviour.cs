//#define DebugCameraLocking

using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class CameraBehaviour : MonoBehaviour
{	
	public GameObject controller;
	public Transform head;
	public Transform twistBone;

	[System.Flags]
	public enum RotationAxes
	{
		MouseX = 1 << 0,
		MouseY = 1 << 1,
		MouseXAndY = MouseX | MouseY,
	}
	/// <summary>
	/// The axes the mouse can be used to rotate.
	/// </summary>
	public RotationAxes mouseRotationAxes = RotationAxes.MouseXAndY;

	public float maxHorizontalRotationDegreesPerSecond = 360.0f;
	public float maxVerticalRotationDegreesPerSecond = 360.0f;

	public float standingMinimumY = -80.0f;
	public float standingMaximumY = 80.0f;
	public float minimumX = -180.0f;
	public float maximumX = 180.0f;

	private float currentMinimumY;
	private float currentMaximumY;

	private float rotationX = 0.0f;
	private float rotationY = 0.0f;

	public bool canControlVertical = true;
	public bool cancontrolHorizontal = true;

	public float mouseSensitivity = 1.0f;
	public bool invertYMotion = true;

	/// <summary>
	/// Camera shaking part
	/// </summary>
	private bool shakeCamera = false;
	private Vector3 shakeAmount = Vector3.zero;
	private float shakeTime = 0.0f;
	private float totalShakeTime = 0.0f;
	private AnimationCurve rollof = null;

	[SerializeField] private bool useMouseXForTwistBone;
	/// <summary>
	/// Should horizontal mouse movement be used to rotate the twist bone?
	/// If false, the player will be rotated instead.
	/// </summary>
	public bool UseMouseXForTwistBone
	{
		get { return useMouseXForTwistBone; }
		set
		{
			if (!value && useMouseXForTwistBone)
			{
				//Changed from true to false.
				Quaternion twistBoneRotation = twistBone.rotation;
				Vector3 twistBoneEulerAngles = twistBoneRotation.eulerAngles;

				//Set player rotation to the Y component of the twist bone rotation since the twist
				//bone is usually only rotated around the X axis (up/down).
				Vector3 newPlayerRotation = new Vector3(0.0f, twistBoneEulerAngles.y, 0.0f);
				if (overridePlayerRotationX)
				{
					newPlayerRotation.x = controller.transform.eulerAngles.x;
				}
				controller.transform.eulerAngles = newPlayerRotation;

				//Set twist bone to only use rotation around the X axis (only up/down), since rotation
				//around Y is applied to the character controller instead.
				twistBone.localEulerAngles = new Vector3(twistBoneEulerAngles.x, 0.0f, 0.0f);

				rotationX = twistBoneEulerAngles.y;
			}
			else if (value && !useMouseXForTwistBone)
			{
				rotationX = 0.0f;
			}

			useMouseXForTwistBone = value;
		}
	}

	[SerializeField] private Transform lookAtTarget = null;
	public Transform LookAtTarget2
	{
		get { return lookAtTarget; }
		set
		{
			if (value == null)
			{
				rotationX = controller.transform.eulerAngles.y;
				rotationY = -twistBone.localEulerAngles.x;
			}
			lookAtTarget = value;
		}
	}
	private float lookAtTargetSpeed = 0.1f;

	public bool canTurnLeft = true;
	public bool canTurnRight = true;

	[HideInInspector] public bool overridePlayerRotationX = false;

	/// <summary>
	/// 1x1 texture used for camera fades. Much cheaper than telling iTween to make its own
	/// texture (it makes one the size of the screen, very expensive).
	/// </summary>
	private Texture2D cameraFadeTexture;

	public bool enableCameraShake = true;

	private float mouseSpeedModifier = 1.0f;

	private float originalStandingMinimumY;
	private float originalStandingMaximumY;
	private float originalMinimumX;
	private float originalMaximumX;


	private void OnEnable()
	{
		originalStandingMinimumY = standingMinimumY;
		originalStandingMaximumY = standingMaximumY;
		originalMinimumX = minimumX;
		originalMaximumX = maximumX;

		rotationX = twistBone.eulerAngles.y;

		cameraFadeTexture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
		cameraFadeTexture.name = "CameraBehaviour fade";
	}

	private void LateUpdate()
	{
		currentMinimumY = standingMinimumY;
		currentMaximumY = standingMaximumY;

		MouseMovement(cancontrolHorizontal, canControlVertical);

		if (shakeCamera)
		{
			shakeTime += Time.deltaTime;
			if (shakeTime >= totalShakeTime)
			{
				totalShakeTime = 0.0f;
				shakeTime = 0.0f;
				shakeCamera = false;
				this.transform.localPosition = Vector3.zero;
			}
			else
			{
				float randomX = Random.Range(-shakeAmount.x, shakeAmount.x);
				float randomY = Random.Range(-shakeAmount.y, shakeAmount.y);
				float randomZ = Random.Range(-shakeAmount.z, shakeAmount.z);
				Vector3 randomPosition = new Vector3(randomX, randomY, randomZ);

				float amplitude = (rollof == null) ? 1.0f : rollof.Evaluate(shakeTime * 0.1f);
				this.transform.localPosition = randomPosition * amplitude;
			}
		}

		if (LookAtTarget2 != null)
		{
			Quaternion lookTowardsTarget = Quaternion.LookRotation((LookAtTarget2.position - twistBone.position).normalized, Vector3.up);
			Quaternion twistBoneLocalRotation = Quaternion.Slerp(twistBone.localRotation, lookTowardsTarget, lookAtTargetSpeed);
			Vector3 twistBoneLocalEuler = Mul(twistBoneLocalRotation.eulerAngles, new Vector3(1.0f, 0.0f, 0.0f));
			twistBone.localEulerAngles = twistBoneLocalEuler;

			Quaternion lookTowardsTarget2 = Quaternion.LookRotation((LookAtTarget2.position - controller.transform.position).normalized, Vector3.up);
			Quaternion playerLocalRotation = Quaternion.Slerp(controller.transform.rotation, lookTowardsTarget2, lookAtTargetSpeed);
			Vector3 playerLocalEuler = Mul(playerLocalRotation.eulerAngles,
			                               new Vector3(overridePlayerRotationX ? 1.0f : 0.0f, 1.0f, 0.0f));
			controller.transform.eulerAngles = playerLocalEuler;
		}
	}

	public Vector3 Mul(Vector3 a, Vector3 other)
	{
		return new Vector3(a.x * other.x, a.y * other.y, a.z * other.z);
	}

	public float DistanceSquared(Vector3 point1, Vector3 point2)
	{
		Vector3 delta = point1 - point2;
		return delta.sqrMagnitude;
	}

	public float ClampDegrees180(float degrees)
	{
		float resultDegrees = degrees;
		while (resultDegrees > 180.0f)
		{
			resultDegrees -= 360.0f;
		}
		while (resultDegrees < -180.0f)
		{
			resultDegrees += 360.0f;
		}
		return resultDegrees;
	}

	public void CameraShake(Vector3 amount, float time, AnimationCurve setRollof = null, float delay = 0F)
	{
		if (!enableCameraShake)
		{
			return;
		}

		amount *= 0.5f;
		shakeAmount = amount;
		shakeTime = 0.0f;
		totalShakeTime = time;
		rollof = setRollof;

		StartCoroutine(DelayedCameraShake(delay));
	}

	public void CameraShake(Vector3 amount, float time, float maxDist, Vector3 target, AnimationCurve setRollof = null, float delay = 0F)
	{
		if (!enableCameraShake)
		{
			return;
		}

		amount *= 0.5f;
		shakeTime = 0.0f;
		totalShakeTime = time;
		rollof = setRollof;

		float dist = DistanceSquared(this.transform.position, target);
		float amp = (maxDist*maxDist) / dist;
		shakeAmount = amount * amp;

		StartCoroutine(DelayedCameraShake(delay));
	}


	private IEnumerator DelayedCameraShake(float delay)
	{
		yield return delay;
		shakeCamera = true;
	}

	/// <summary>
	/// Looks at the target transform for the duration specified.
	/// </summary>
	/// <param name="target">The target to look at.</param>
	/// <param name="time">The duration the camera should look at the target for.</param>
	/// <param name="speed">The speed with which to move the camera, where numbers near 0 moves the
	/// camera slowly, and numbers near 1 moves it very fast (1 snaps to target instantly).</param>
	public void LookAtTarget(Transform target, float time, float speed = 0.1f)
	{
		LookAtTarget2 = target;
		//Start the timed stop of the LookAt. Each coroutine is assigned a unique ID, so that only
		//the most recently launched coroutine will stop the LookAt. This is done to prevent one
		//LookAt from ruining the next one if their durations overlap.
		currentTimedRemoveID = ++timedRemoveIDGenerator;
		StartCoroutine(TimedRemoveLookAtTarget(time, currentTimedRemoveID));
		lookAtTargetSpeed = speed;
	}

	private int timedRemoveIDGenerator = 0;
	private int currentTimedRemoveID = 0;
	private IEnumerator TimedRemoveLookAtTarget(float time, int ID)
	{
		yield return new WaitForSeconds(time);
		if (ID == currentTimedRemoveID)
		{
			LookAtTarget2 = null;
		}
	}

	public void FadeCameraFrom(Color color, float time, float delay, float transparency = 1F)
	{
		cameraFadeTexture.SetPixel(0, 0, color);
		cameraFadeTexture.Apply();
		iTween.CameraFadeAdd(cameraFadeTexture);
		iTween.CameraFadeFrom(iTween.Hash("amount", transparency, "time", time, "delay", delay));
	}

	public void FadeCameraTo(Color color, float time, float delay, float transparency = 1F)
	{
		cameraFadeTexture.SetPixel(0, 0, color);
		cameraFadeTexture.Apply();
		iTween.CameraFadeAdd(cameraFadeTexture);
		iTween.CameraFadeTo(iTween.Hash("amount", transparency, "time", time, "delay", delay));
	}

	public bool useDeltaTimeForMouseInput = false;

	//EXTRA is a minor adjustment of the angular velocity of the ground when it is applied
	//to the rotation of the camera. 0.9549305 seems to be the magic value which makes
	//everything work at 60 physics FPS.
	public bool useEXTRA = true;
	public bool autoEXTRA = true;
	public float EXTRA = 0.9549305f;

	private bool lookedAtLastFrame = false;
	/*
	 * Disable this to disable mouse movement
	 **/
	private void MouseMovement(bool horizontalEnabled, bool verticalEnabled)
	{
		if (LookAtTarget2)
		{
			lookedAtLastFrame = true;
			return;
		}
		else if (lookedAtLastFrame)
		{
			//Lookat target removed, set vertical camera rotation so that the camera will
			//change to free look mode smoothly.
			lookedAtLastFrame = false;

			SetRotationY(-twistBone.localEulerAngles.x);
		}

		Vector2 mouseDelta;
		{
			mouseDelta = new Vector2(Input.GetAxis("Mouse X") + Input.GetAxis("Joystick X"), Input.GetAxis("Mouse Y") + Input.GetAxis("Joystick Y")) * mouseSensitivity * mouseSpeedModifier;
		}
		if (useDeltaTimeForMouseInput)
		{
			mouseDelta *= Time.deltaTime * 60.0f;
		}

		if (!horizontalEnabled)
		{
			mouseDelta.x = 0.0f;
		}
		if (!verticalEnabled)
		{
			mouseDelta.y = 0.0f;
		}

		if (invertYMotion)
		{
			mouseDelta.y *= -1.0f;
		}

		if ((mouseRotationAxes & RotationAxes.MouseX) != 0 && horizontalEnabled)
		{
			Transform rotationTarget = useMouseXForTwistBone ? twistBone : controller.transform;
			if (!useMouseXForTwistBone)
			{
				rotationX = controller.transform.eulerAngles.y;
			}
			rotationX += GetHorizontalRotation(mouseDelta.x);

			rotationX = ClampDegrees180(rotationX);
			
			rotationX = Mathf.Clamp(rotationX, minimumX, maximumX);
			rotationTarget.localEulerAngles = new Vector3(rotationTarget.localEulerAngles.x, rotationX, 0.0f);
		}
		if ((mouseRotationAxes & RotationAxes.MouseY) != 0 && verticalEnabled)
		{
			rotationY += GetVerticalRotation(mouseDelta.y);
			rotationY = Mathf.Clamp(rotationY, currentMinimumY, currentMaximumY);

			twistBone.localEulerAngles = new Vector3(-rotationY, twistBone.localEulerAngles.y, 0.0f);
		}
	}

	private float GetHorizontalRotation(float mouseXDelta)
	{
		if (mouseXDelta < 0.0f && !canTurnLeft ||
			mouseXDelta > 0.0f && !canTurnRight)
		{
			return 0.0f;
		}

		float maxRotation = maxHorizontalRotationDegreesPerSecond * Time.deltaTime;
		float rotationX = Mathf.Clamp(mouseXDelta, -maxRotation, maxRotation);
		return rotationX;
	}

	private float GetVerticalRotation(float mouseYDelta)
	{
		float maxRotation = maxVerticalRotationDegreesPerSecond * Time.deltaTime;
		float rotationY = Mathf.Clamp(mouseYDelta, -maxRotation, maxRotation);
		return rotationY;
	}

	public void SetAngleLimits(float minX = -180.0f, float maxX = 180.0f, float minY = -80.0f, float maxY = 80.0f)
	{
#if DebugCameraLocking
		Debug.Log(string.Format("Setting camera limits to x=[{0:N2}, {1:N2}], y=[{2:N2}, {3:N2}]",
			minX, maxX, minY, maxY));
#endif

		minimumX = minX;
		maximumX = maxX;

		standingMinimumY = minY;
		standingMaximumY = maxY;
	}

	public void ResetHorizontalLimits()
	{
		minimumX = originalMinimumX;
		maximumX = originalMaximumX;
	}

	public void ResetVerticalLimits()
	{
		standingMinimumY = originalStandingMinimumY;
		standingMaximumY = originalStandingMaximumY;
	}

	public void SetRotationY(float rotY)
	{
		float newRotationY = ClampDegrees180(rotY);
		rotationY = newRotationY;
	}

	public float GetRotationY()
	{
		return rotationY;
	}

	/// <summary>
	/// Sets a modifier which will be used for mouse rotation. This can be used to make
	/// camera turning slower or faster.
	/// </summary>
	/// <param name="mouseSpeedModifier">The modifier, where 1 is normal speed.</param>
	public void SetMouseSpeedModifier(float mouseSpeedModifier)
	{
		this.mouseSpeedModifier = mouseSpeedModifier;
	}
}