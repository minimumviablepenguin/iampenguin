﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour
{
	[SerializeField] private string gameScene = "Game";

	public void Update()
	{
		Screen.lockCursor = true;
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void Start()
	{
		if (Time.realtimeSinceStartup < 0.5f)
		return;

			Application.LoadLevel(gameScene);
	}
}
