using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Conversation : MonoBehaviour
{
	public enum Waits
	{
		Time, Audio, Animation, Subtitle
	}
	public enum Speakers
	{
		Player, NPC
	}

	public enum AudioVoiceMood
	{
		None, Angry, Sad, Chatter, Question, Shout
	}

	[Serializable]
	public class DialogItem
	{
		public bool show = true;
		public Speakers speaker = Speakers.NPC;
		public AudioClip spoken = null;
		public string subtitle = "(enter subtitle text)";
		public AnimationClip player_animation = null;
		public AnimationClip npc_animation = null;
		public Waits wait = Waits.Time;
		public AudioVoiceMood audioVoiceMood = AudioVoiceMood.None;
		public bool show_camera = true;

		public bool is_branch = false;
		public List<String> choice;
		public List<int> jump;

		public bool addDialogJump = false;
		public int dialogJump = 0;

		public bool has_action = false;
		public GameObject ActionReceiver;
		public String ActionMethod;
		public String ActionParameter;

		public bool endDialog = false;

		public float fallBackDelay = 3.0f;

		public DialogItem(bool branch = false)
		{
			if (branch)
			{
				is_branch = true;
			}
		}
	}

	public List<DialogItem> dialog;

	// other variables
	public bool Repeat = false; // whether or not we repeat the conversation when the player comes a 2nd time

	private bool firsttime = true;
	private GameObject Player;
	private PenguinAgent penguin;

	public static bool inConversation = false;
	public static Conversation activeConversation;
	private bool talking = false;
	private bool branching = false;
	private int index = 0;
	private bool CursorLocked = false;

	private bool isInsideTrigger = false;

	void Start()
	{
		Player = AssetManager.Player;
		penguin = transform.parent.parent.GetComponent<PenguinAgent>();
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.attachedRigidbody == Player.GetComponent<Rigidbody>())
			isInsideTrigger = true;
	}

	void OnTriggerExit(Collider col)
	{
		if (col.attachedRigidbody == Player.GetComponent<Rigidbody>())
			isInsideTrigger = false;
	}

	public void FirstActionClicked()
	{
		if (talking || branching)
		{
			if (talking)
			{
			}
			else
			{
				index = dialog[index].jump[0];
				branching = false;
				if (index == -1)
				{
					StopAllCoroutines();
					ReturnToScene();
				}
			}
		}
	}

	public void SecondActionClicked()
	{
		if (talking || branching)
		{
			if (talking)
			{
			}
			else
			{
				index = dialog[index].jump[1];
				branching = false;
				if (index == -1)
				{
					StopAllCoroutines();
					ReturnToScene();
				}
			}
		}
	}


	void Update()
	{
		if ((!inConversation && isInsideTrigger && (firsttime || Repeat)) && (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Action1")))
		{
			inConversation = true;
			firsttime = false;

			StartCoroutine(StartConversation());
		}
		if (inConversation && activeConversation == this)
		{
			AssetManager.CameraBehaviour.LookAtTarget(this.transform, 0.1f);
		}
	}


	IEnumerator StartConversation()
	{
		if (dialog != null)
		{
			activeConversation = this;
			Player.SendMessageUpwards("SetControllable", false);
			gameObject.SendMessageUpwards("StartConversationActions", SendMessageOptions.DontRequireReceiver);

			// run the conversation tree		
			for (index = 0; index < dialog.Count; index++)
			{
				DialogItem d = dialog[index];

				AssetManager.Subtitle.text = "";
				AssetManager.Subtitle.gameObject.SetActive(false);
				AssetManager.Choices[0].GetComponent<UIButtonKeys>().startsSelected = true;

				foreach (var choice in AssetManager.Choices)
				{
					choice.text = "";
					choice.gameObject.SetActive(false);
				}
				
				if (d.is_branch)
				{
					Debug.Log("BRANCH");
					for (int i = 0; i < dialog[index].choice.Count; i++)
					{
						AssetManager.Choices[i].gameObject.SetActive(true);
						AssetManager.Choices[i].text = d.choice[i];
					}
					
					if (CursorLocked) Screen.lockCursor = false; // unlock the cursor, because we need it
					branching = true; talking = false;
					yield return StartCoroutine(Branch(d, index));
					index--; // correct that the for loop will increment it
					if (CursorLocked) Screen.lockCursor = true; // lock the cursor, if it was locked
				}
				else
				{
					Debug.Log("SUB");
					AssetManager.Subtitle.gameObject.SetActive(true);
					AssetManager.Subtitle.text = d.subtitle;
					talking = true; branching = false;
					yield return StartCoroutine(Talk(d));
					Debug.Log("END OF SUB");
				}
			} // endof loop over all dialog elements

			ReturnToScene();
		}
	}

	IEnumerator Talk(DialogItem d)
	{
		float WaitTime = 0f;

		switch (d.wait)
		{
			case Waits.Time:
				WaitTime = d.fallBackDelay;
				break;
			case Waits.Audio:
				if (d.spoken != null) WaitTime = d.spoken.length;
				break;
			case Waits.Animation:
				if (d.speaker == Speakers.Player)
				{
					if (d.player_animation) WaitTime = d.player_animation.length;
				}
				else
				{
					if (d.npc_animation) WaitTime = d.npc_animation.length;
				}
				break;
		}
		// fallback - same as Waits.Subtitle
		if (WaitTime == 0f) WaitTime = d.fallBackDelay;//(float)d.subtitle.Length / 12f;
		// spoken text audio output
		if (d.spoken)
		{
			GetComponent<AudioSource>().enabled = true;
			GetComponent<AudioSource>().clip = d.spoken;
			GetComponent<AudioSource>().Play();
		}

		if (d.audioVoiceMood != AudioVoiceMood.None)
		{
			switch (d.audioVoiceMood)
			{
				case AudioVoiceMood.Chatter:
					penguin.PlayRandomChatterSound();
					break;
				case AudioVoiceMood.Angry:
					penguin.PlayRandomAngrySound();
					break;
				case AudioVoiceMood.Question:
					penguin.PlayRandomQuestionSound();
					break;
				case AudioVoiceMood.Shout:
					penguin.PlayRandomShoutSound();
					break;
				case AudioVoiceMood.Sad:
					penguin.PlayRandomSadSound();
					break;
			}
		}

		// animations
		if (d.player_animation && Player.GetComponent<Animation>() != null) Player.GetComponent<Animation>().CrossFade(d.player_animation.name);
		if (d.npc_animation && GetComponent<Animation>() != null) GetComponent<Animation>().CrossFade(d.npc_animation.name);

		float endWaitTime = Time.time + WaitTime;
		float possibleToSkipTime = Time.time + 0.5f;

		// wait until we are done before doing next in loop
		while (Time.time < endWaitTime)
		{
			if (Time.time > possibleToSkipTime && Input.anyKeyDown)
			{
				break;
			}
			yield return null;
		}

		if (d.has_action && !string.IsNullOrEmpty(d.ActionMethod))
		{
			if (d.ActionReceiver)
			{
				d.ActionReceiver.SendMessage(d.ActionMethod, d.ActionParameter);
			}
			else
			{
				gameObject.SendMessageUpwards(d.ActionMethod, d.ActionParameter);
			}
		}

		if (d.endDialog)
		{
			StopAllCoroutines();
			ReturnToScene();
		}

		if (d.addDialogJump)
		{
			index = d.dialogJump - 1;
		}
	}

	IEnumerator Branch(DialogItem d, int i)
	{
		while (index == i)
		{
			yield return null;
		}
		Debug.Log("Done branching");
	}

	void ReturnToScene()
	{
		AssetManager.CameraBehaviour.LookAtTarget(null, 0.0f);

		AssetManager.Subtitle.text = "";
		AssetManager.Subtitle.gameObject.SetActive(false);
		AssetManager.Choices[0].GetComponent<UIButtonKeys>().startsSelected = true;

		foreach (var choice in AssetManager.Choices)
		{
			choice.text = "";
			choice.gameObject.SetActive(false);
		}

		talking = false;
		inConversation = false;
		activeConversation = null;
		gameObject.SendMessageUpwards("StopConversationActions", SendMessageOptions.DontRequireReceiver);

		Player.SendMessageUpwards("SetControllable", true);

		// back to the default animation - since we're making a camera cut, no need to fade
		if (GetComponent<Animation>() != null)
		{
			GetComponent<Animation>().Stop();
			GetComponent<Animation>().Play();
		}
	}
}