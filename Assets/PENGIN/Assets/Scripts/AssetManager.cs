using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[AddComponentMenu("Krillbite/Managers/AssetManager")]
public class AssetManager : MonoBehaviour
{
	private static AssetManager instance = null;
	public static AssetManager getInstance
	{
		get
		{
			if (!Exist)
			{
				// Checks if an instance exists in stage.
				instance = (AssetManager)FindObjectOfType(typeof(AssetManager));

				// If no instance is found, create one on an empty gameobject.
				if (!Exist)
				{
					GameObject go = new GameObject();
					instance = go.AddComponent<AssetManager>();
					go.name = "AssetManagerSingleton";
					Debug.LogWarning("Instantiated the \"AssetManager\" on an empty stage object.");
				}
			}

			return instance;
		}
	}
	public static bool Exist { get { return instance != null; } }

	public static GameObject Player { get { return getInstance.player; } }
	public static Camera MainCamera { get { return getInstance.mainCamera; } }
	public static CameraBehaviour CameraBehaviour { get { return getInstance.cameraBehaviour; } }
	public static UILabel Subtitle { get { return getInstance.subtitle; } }
	public static UILabel[] Choices { get { return getInstance.choices; } }

	public GameObject player;
	public Camera mainCamera;
	public CameraBehaviour cameraBehaviour;

	public UILabel subtitle;
	public UILabel[] choices;

	public static AudioListener AudioListener { get { return getInstance.audioListener; } }
	public AudioListener audioListener;

	private void SetInstance()
	{
		if (!Exist)
			instance = this;
	}
	private void Reset()
	{
		SetInstance();
		FindElements();
	}

	private void Awake()
	{
		SetInstance();
	}

	public void FirstChoiceClicked()
	{
		Conversation.activeConversation.FirstActionClicked();
	}

	public void SecondChoiceClicked()
	{
		Conversation.activeConversation.SecondActionClicked();
	}

	public void FindElements()
	{
		GameObject obj;

		if (player == null)
		{
			obj = GameObject.FindGameObjectWithTag("Player");

			if (obj != null)
				player = obj;
		}

		if (mainCamera == null)
		{
			obj = GameObject.FindGameObjectWithTag("MainCamera");
			if (obj != null)
				mainCamera = obj.GetComponent<Camera>();
		}
		
		if (audioListener == null)
		{
			obj = GameObject.Find("PlayerAudioListener");
			if (obj != null)
				audioListener = obj.GetComponent<AudioListener>();
		}

		if (cameraBehaviour == null)
		{
			obj = FindObjectOfType<CameraBehaviour>().gameObject;
			if (obj != null)
				cameraBehaviour = obj.GetComponent<CameraBehaviour>();
		}
	}
}
