﻿using UnityEngine;
using System.Collections;
//using Fabric;

public class PenguinController : MonoBehaviour
{
	[SerializeField] private float forwardSpeed = 5.0f;
	[SerializeField] private float sidewaysSpeed = 5.0f;
	[SerializeField] private float minDistanceBeforeDie = 8.0f;
	public float health = 100.0f;
	public float damageWhenAloneAmplitude = 1.0f;

	private float m_Speed = 0;
	private float m_Direction = 0;

	[SerializeField] private bool controllable = true;
	[SerializeField] private bool dead = false;

//	public RandomComponent audioShout;
//	public AudioSource audioDieMusic;

	private float startHealth;
	private SkinnedMeshRenderer meshRenderer;
	private SkinnedMeshRenderer iceMeshRenderer;

	public Texture2D crosshair;

	private Animator m_Animator = null;

	private int m_SpeedId = 0;
	private int m_AgularSpeedId = 0;
	private int m_DirectionId = 0;

	private float m_SpeedDampTime = 0.1f;
	private float m_AnguarSpeedDampTime = 0.25f;
	private float m_DirectionResponseTime = 0.2f;

	public void Do(float speed, float direction)
	{
		AnimatorStateInfo state = m_Animator.GetCurrentAnimatorStateInfo(0);

		bool inTransition = m_Animator.IsInTransition(0);
		bool inIdle = state.IsName("Locomotion.peng_idle");
		bool inSideHop = state.IsName("Locomotion.peng_side_hop");
		bool inWalk = state.IsName("Locomotion.peng_walk_forward");

		float speedDampTime = inIdle ? 0 : m_SpeedDampTime;
		float angularSpeedDampTime = inWalk || inTransition ? m_AnguarSpeedDampTime : 0;
		float directionDampTime = inSideHop || inTransition ? 1000000 : 0;

		float angularSpeed = direction / m_DirectionResponseTime;

		m_Animator.SetFloat(m_SpeedId, speed, speedDampTime, Time.deltaTime);
		m_Animator.SetFloat(m_AgularSpeedId, angularSpeed, angularSpeedDampTime, Time.deltaTime);
		m_Animator.SetFloat(m_DirectionId, direction, directionDampTime, Time.deltaTime);
	}	

	IEnumerator Start()
	{
		m_Animator = GetComponentInChildren<Animator>();
		m_Animator.logWarnings = false;

		meshRenderer = transform.FindChild("Penguin").GetComponent<SkinnedMeshRenderer>();
		iceMeshRenderer = transform.FindChild("Ice_Crystal_101").GetComponent<SkinnedMeshRenderer>();
		iceMeshRenderer.enabled = false;
		startHealth = health;

		m_SpeedId = Animator.StringToHash("Speed");
		m_AgularSpeedId = Animator.StringToHash("AngularSpeed");
		m_DirectionId = Animator.StringToHash("Direction");

		float iceFactor = (100.0f - health) * 0.01f;
		meshRenderer.material.SetFloat("_IceAmount", iceFactor);
		Camera.main.GetComponent<IcyOverlay>().overlayIntensity = iceFactor;

		if (crosshair == null)
		{
			crosshair = new Texture2D(5, 5);
			for (var i = 0; i < crosshair.width; i++)
			{
				for (var j = 0; j < crosshair.height; j++)
				{
					crosshair.SetPixel(i, j, Color.white);
				}
			}
			crosshair.Apply();
		}

		AssetManager.CameraBehaviour.FadeCameraTo(Color.white, 0.0f, 0.0f);
		yield return new WaitForSeconds(0.1f);
		AssetManager.CameraBehaviour.FadeCameraTo(Color.white, 3.0f, 0.0f, 0.0f);
	}

	void Update()
	{
		Screen.lockCursor = true;

		if (dead || !controllable)
			return;

		if (health <= 0)
		{
			StartCoroutine(Die());
			return;
		}

		if (health < startHealth)
		{
			float iceFactor = (100.0f - health) * 0.01f;
			meshRenderer.material.SetFloat("_IceAmount", iceFactor);
//			audioDieMusic.volume = iceFactor * 10;
			Camera.main.GetComponent<IcyOverlay>().overlayIntensity = iceFactor;
		}

		CheckForSurvivors();

		if (Input.GetButtonDown("Jump"))
		{
			m_Animator.SetTrigger("Jump");
		}

		var horizontal = Input.GetAxis("Horizontal") * sidewaysSpeed;
		var vertical = Input.GetAxis("Vertical") * forwardSpeed;

		m_Speed = vertical;
		m_Direction = horizontal;

		Do(m_Speed * 6, m_Direction * 180);

		if (Input.GetKey(KeyCode.LeftControl))
		{
			RaycastHit hitInfo;
			if (Physics.Raycast(transform.position + Vector3.up * 0.5f, transform.forward, out hitInfo))
			{
				hitInfo.collider.SendMessageUpwards("HitPenguin", 1);
			}
		}
	}

	private void CheckForSurvivors()
	{
		var closestPenguin = Mathf.Infinity;
		var lastPenguin = true;

		foreach (var penguinAgent in PenguinAgent.allPenguins)
		{
			if (penguinAgent == this || penguinAgent.dead)
				continue;

			var distanceBetween = Vector3.Distance(transform.position, penguinAgent.transform.position);
			if (distanceBetween < closestPenguin)
			{
				closestPenguin = distanceBetween;
			}
			if (!penguinAgent.dead)
			{
				lastPenguin = false;
			}
		}

		if (lastPenguin || closestPenguin > minDistanceBeforeDie)
		{
			health = Mathf.Clamp(health - Time.deltaTime*damageWhenAloneAmplitude, 0.0f, startHealth);
		}
		else
		{
			health = Mathf.Clamp(health + Time.deltaTime * damageWhenAloneAmplitude, 0.0f, startHealth);
		}
	}

	void OnGUI()
	{
		GUI.DrawTexture(
			new Rect(Screen.width * 0.5f - (crosshair.width * 0.5f), Screen.height * 0.5f - (crosshair.height * 0.5f), crosshair.width,
					 crosshair.height), crosshair);
	}

	IEnumerator Die()
	{
		Debug.Log(gameObject.name + " died");

		Do(0, 0);
		m_Animator.speed = 0;
		iceMeshRenderer.enabled = true;
		GetComponent<Rigidbody>().isKinematic = true;
		controllable = false;
		dead = true;
		AssetManager.CameraBehaviour.enabled = false;

		yield return new WaitForSeconds(5.0f);
		AssetManager.CameraBehaviour.FadeCameraTo(Color.white, 3.0f, 0.0f);
		yield return new WaitForSeconds(2.5f);
		Application.LoadLevel("MainMenu");
	}

	public void SetControllable(bool value)
	{
		controllable = value;
	}

/*	public void RandomAudioShout()
	{
		if (audioShout.IsPlaying())
			return;

		audioShout.Play(audioShout.CreateInstance(audioShout.gameObject));
	}*/
}
