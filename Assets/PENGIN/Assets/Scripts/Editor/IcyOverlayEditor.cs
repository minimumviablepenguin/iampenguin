using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(IcyOverlay))]
public class IcyOverlayEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.DrawDefaultInspector();

		if (GUILayout.Button("I am a button."))
		{
			Debug.Log("Don't push me, bro.");
		}
	}
}
