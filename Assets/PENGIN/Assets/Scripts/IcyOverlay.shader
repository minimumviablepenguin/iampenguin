Shader "Hidden/Icy Overlay" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_OverlayTex ("Overlay Texture", 2D) = "white" {}
}

SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }
				
		CGPROGRAM
		// #pragma target 3.0
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest 
		#include "UnityCG.cginc"

		struct v2f {
			float4 pos	: POSITION;

			float2 uv : TEXCOORD0;
		};
			
		uniform sampler2D _MainTex;
		uniform sampler2D _OverlayTex;
		uniform float overlayIntensity;

		v2f vert (appdata_img v)
		{
			v2f o;
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			o.uv = MultiplyUV (UNITY_MATRIX_TEXTURE0, v.texcoord);
			return o;
		}

		float4 frag (v2f i) : COLOR
		{
			float4 original = tex2D(_MainTex, i.uv);
			float4 overlay = tex2D(_OverlayTex, i.uv);
			float4 col = lerp(original, overlay, overlay.aaaa * overlayIntensity);
			return col;
			}
		ENDCG
		}
	}
Fallback off
}